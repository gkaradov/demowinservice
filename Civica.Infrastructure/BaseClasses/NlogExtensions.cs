﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NLog;
using System.Diagnostics;
using System.IO;

namespace Civica.Infrastructure.BaseClasses
{
    public class DCLogger 
    {

        public DCLogger()
        {
         
        }

        public Logger NLog { get; set; }


        public void CreateTaskLog(LogLevel  logLevel, string message, int taskId, int  UserId, string FilePath, int SiteId, string Sensors)
        {
           
            LogEventInfo theEvent = new LogEventInfo(logLevel, "", message);
            theEvent.Properties["TaskId"] = taskId;
            theEvent.Properties["UserId"] = taskId;
            theEvent.Properties["FilePath"] = taskId;
            theEvent.Properties["SiteId"] = taskId;
            theEvent.Properties["Sensors"] = taskId;

            AddSystemInfoAndWriteLog(theEvent);
        }


        private void AddSystemInfoAndWriteLog(LogEventInfo theEvent)
        {
           // var log = new DCLogger();

            PerformanceCounter cpuCounter;
            PerformanceCounter ramCounter;

            cpuCounter = new PerformanceCounter();

            cpuCounter.CategoryName = "Processor";
            cpuCounter.CounterName = "% Processor Time";
            cpuCounter.InstanceName = "_Total";

            ramCounter = new PerformanceCounter("Memory", "Available MBytes");

            theEvent.Properties["CPU"] = cpuCounter.NextValue() + "%";
            theEvent.Properties["Memory"] = ramCounter.NextValue() + "MB";

            var driverInfo = String.Empty;
            DriveInfo[] drives = DriveInfo.GetDrives();
            foreach (DriveInfo drive in drives)
            {
                //There are more attributes you can use.
                //Check the MSDN link for a complete example.
                driverInfo = driverInfo + "Name :"+ drive.Name + ", ";
                driverInfo = driverInfo + "Volume :" + drive.VolumeLabel + ", ";
                driverInfo = driverInfo + "Type :" + drive.DriveType + ", ";
                driverInfo = driverInfo + "Format :" + drive.DriveFormat + " ";

                if (drive.IsReady)
                {
                    driverInfo = driverInfo + "Ready : Yes" ;
                }
                else
                {
                    driverInfo = driverInfo + "Ready : No";
                }

                driverInfo = driverInfo + "TotalSize :" + drive.TotalSize + " ";
                driverInfo = driverInfo + "TotalFreeSpace :" + drive.TotalFreeSpace + " ";
                driverInfo = driverInfo + "AvailableFreeSpace :" + drive.AvailableFreeSpace + " ;";
            }

            theEvent.Properties["Disk"] = driverInfo;
             NLog.Log(theEvent);

        }

    }
}
