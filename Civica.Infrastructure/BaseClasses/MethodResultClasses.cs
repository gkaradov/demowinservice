﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Civica.Infrastructure.BaseClasses
{
    public class FileResult
    {
        public FileResult()
        {
            Messages = new List<String>();
        }


        public FileStream File { get; set;}
        public StreamReader TextStream { get; set; }
        public bool Exists { get; set; }
        public List<String> Messages { get; set; }
    }


    public class DirectoryResult
    {
        public DirectoryResult()
        {
            Messages = new List<String>();
            Files = new List<DirectoryFiles>();
        }

        public List<DirectoryFiles> Files { get; set; }
        public bool Exists { get; set; }
        public int FileCount { get; set; }
        public List<String> Messages { get; set; }
    }


    public class DirectoryFiles
    {
        public string FilePath { get; set; }
        public string RootDirectory { get; set; }

    }


        public class MethodyResult
    {
        public bool Sucess { get; set; }
        public List<String> Messages { get; set; }
    }



    [Serializable]
    public abstract class BaseServiceResultDTO
    {
        protected BaseServiceResultDTO()
        {
            ErrorResults = new Dictionary<string, string>();
            ServiceLogs = new Dictionary<string, string>();
            ToBeMoved = false;
        }

        // Code your save responses against IsSuccess, we can change how it's determined later if needed.
        public bool IsSuccess { get { return ErrorResults.Count == 0; } }
        public IDictionary<string, string> ErrorResults { get; protected set; }
        public IDictionary<string, string> ServiceLogs { get; protected set; }
        public bool ToBeMoved { get; set; }

        public abstract string ServiceType { get; }

        public void AddErrorResults(IDictionary<string, string> errors)
        {
            ErrorResults = errors;
        }

        public void AddError(string key, string message)
        {
            this.ErrorResults.Add(key, message);
        }

        public void AddServiceLogResults(IDictionary<string, string> log)
        {
            ServiceLogs = log;
        }

        public void AddServiceLog(string key, string message)
        {
            this.ServiceLogs.Add(key, message);
        }

    }

    public class ServiceResult<T> : BaseServiceResultDTO
    {
        private T _DTO;

        public ServiceResult(T dto)
            : base()
        {
            _DTO = dto;
        }

        public T DTO
        {
            get { return _DTO; }
            set { _DTO = value; }
        }

        public override string ServiceType
        {
            get { return typeof(T).FullName; }
        }
    }

    public interface IServiceResult<T>
    {
        bool IsSuccess { get; }
        IDictionary<string, string> ErrorResults { get; }
        IDictionary<string, string> ServiceLogs { get; }
        T DTO { get; set; }
    }



}
