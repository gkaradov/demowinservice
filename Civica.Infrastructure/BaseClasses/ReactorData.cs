﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.Threading.Tasks;

namespace Civica.Infrastructure.BaseClasses
{
    public class ModemInfo
    {
        public Int64 IMEI;
        public IPAddress ip_addr;
    }


    public class ModemData
    {
        public Int64 IMEI;
        public int DI1;
        public int DI2;
        public int DI3;
        public int DI4;

        public double Power;
        public double AI1;
        public double AI2;
        public double AI3;

        public DateTimeOffset TimeStamp;
    }


}

