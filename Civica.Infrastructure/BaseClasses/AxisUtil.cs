﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;

namespace Civica.Infrastructure.BaseClasses
{
    public class AxisUtil
    {
        public static double BitsToDecimal(byte[] TestByte)
        {
            BitArray TestBitArray = new BitArray(TestByte);
            double Mantissa = 0;
            double Exponent;
            int Counter;
            double Result;

            for (Counter = 0; Counter <= 22; Counter++)
            {
                Mantissa = Mantissa + (TestBitArray[Counter] ? 1 : 0) / (System.Math.Pow(2, (-(Counter - 23))));
            }
            Mantissa = Mantissa + 1;
            Exponent = 0;
            for (Counter = 23; Counter <= 30; Counter++)
            {
                Exponent = Exponent + (TestBitArray[Counter] ? 1 : 0) * System.Math.Pow(2, (Counter - 23));
            }
            Exponent = Exponent - 127;

            Result = Mantissa * System.Math.Pow(2, Exponent);
            if (Result < 0.0001)
            {
                return 0;
            }
            else
            {
                return Result;
            }
        }

        public static bool IsNumeric(object Expression)
        {
            bool isNum;
            double retNum;
            isNum = Double.TryParse(Convert.ToString(Expression),
                                    System.Globalization.NumberStyles.Any,
                                    System.Globalization.NumberFormatInfo.InvariantInfo,
                                    out retNum);
            return isNum;
        }

        /// <summary>
        /// Count the number of substring occurrences
        /// </summary>
        /// <param name="haystack">The string to search in</param>
        /// <param name="needle">The substring to search for</param>
        public static int substr_count(string haystack, string needle)
        {
            int x;
            int result = 0;

            if (haystack == "") return 0;
            if (needle == "") return 0;

            int haystack_len = haystack.Length;
            int needle_len = needle.Length;

            for (x = 0; x < haystack_len; x++)
            {
                if (haystack.Substring(x, needle_len) == needle)
                {
                    result++;
                }
            }
            return result;
        }
    }
}
