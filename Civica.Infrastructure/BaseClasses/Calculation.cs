﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;

namespace Civica.Infrastructure.BaseClasses
{
    /// <summary>
    /// A Math Expression Evaluator written by Michael Combs
    /// </summary>
    /// <remarks>
    ///    Introduction
    ///
    ///    A mathematical expression evaluator can be a useful piece of code if you often
    ///    write applications which rely on any kind of scripting. Graphics applications
    ///    which create composite images from templates, form filling or spreadsheet
    ///    software which performs configurable calculations based on user input, or data
    ///    analysis applications that perform calculations on batch data from scripts are
    ///    just a few that come to my mind.
    ///
    ///    Some knowledge of lexical analysis, state machines and parsing would be helpful,
    ///    but not necessary. The brief discussion here and a little experimentation with
    ///    the code in the debugger, should hopefully provide adequate explanation to at
    ///    least get started using the code.
    ///
    ///
    ///    Lexical scanning
    ///
    ///    The first step in evaluating an expression is to identify the individual
    ///    components, or tokens, of the expression. This evaluator uses a finite state
    ///    machine based lexical scanner to identify tokens, assigning each a category such
    ///    as number, operator, punctuation, or function. A state machine uses a set of
    ///    predefined rules to make transitions between states, based on the current state
    ///    and input (characters from a buffer). It will eventually reach an end state
    ///    (let's hope), at which point, end state specific code can be executed. In this
    ///    case, an end state signals that a token has been found, and end state specific
    ///    code identifies it within the input buffer and stores it to a list of tokens.
    ///
    ///    State machines are typically driven by a table like the one below, which is used
    ///    in the code presented in this article. The state is indicated on each row, and
    ///    the column is determined by the input character. The end states are those states
    ///    in which all entries are 1. When one of these end states is reached, the code,
    ///    having tracked its start position and its current position, cuts out the token
    ///    from the buffer, stores it to a list with an associated type (number, operator,
    ///    etc.) and then returns to the start state, state one.
    ///
    ///    For example, let's start with a buffer of "73 " (a space is added as an end
    ///    marker). The transition would be as follows: from State 1, an input of 7
    ///    (number) indicates a move to state 4. From state 4, an input of 3 (number)
    ///    indicates staying in state 4. From state 4, an input of ' ' (space) indicates a
    ///    move to state 5. State 5 is the end state for a number. At this point the number
    ///    73 has been identified, which would then be stored in a list of tokens.
    ///
    ///    State   Letter  Number  Tab   Space   .   Punctuation  Operator
    ///      1       2       4      1       1    4       6           7
    ///      2       2       3      3       3    3       3           3
    ///      3       1       1      1       1    1       1           1
    ///      4       2       4      5       5    4       5           5
    ///      5       1       1      1       1    1       1           1
    ///      6       1       1      1       1    1       1           1
    ///      7       1       1      1       1    1       1           1
    ///
    ///    You might have noticed a little cheating on the column marked Operator.
    ///    Ordinarily, each operator might have its own column, directing the state machine
    ///    when that operator character is input. However, single character operators can
    ///    be combined, provided that some special handling to set the column correctly, is
    ///    added to the code. This was done so that new operators could easily be added
    ///    without any modification to the state table. More on this later.
    ///
    ///
    ///    Parsing and evaluation
    ///
    ///    Once a list of tokens has been generated, each assigned an appropriate type
    ///    (operator, number, etc), the expression is parsed and evaluated. Parsing
    ///    verifies the syntax of the expression, restructures the expression to account
    ///    for operator precedence and, in this case, also evaluates the expression. This
    ///    code uses a relatively simple recursive descent parsing algorithm.
    ///
    ///    Recursive descent parsing is implemented through a series of recursive
    ///    functions, each function handling a different portion of the syntax of an
    ///    expression. The virtue of recursive decent parsing is that, it is easy to
    ///    implement. The primary drawback though is that, the language of the expression,
    ///    math in this case, is hard coded into the structure of the code. As a
    ///    consequence, a change in the language often requires that the code itself be
    ///    modified. There are standard parsing algorithms driven by tables, rather than
    ///    functions, but typically require additional software to generate portions of the
    ///    code and the tables, and can require much greater effort to implement.
    ///
    ///    However, the recursive descent parser used in this code has been written in a
    ///    manner that will allow language modifications typical of those in math
    ///    expressions (functions and operators), with no changes to the structure of the
    ///    code. Adding new operators and functions
    ///
    ///    This code handles most of the basic operators and functions normally encountered
    ///    in an expression. However, adding support for additional operators and functions
    ///    can be implemented simply.
    ///
    ///    The recursive descent parsing functions have been coded in a series of levels,
    ///    each level handling operators of a particular precedence, associativity (left,
    ///    or right) and what might be referred to as degree (unary, or binary). There are
    ///    3 levels of precedence (level1, level2 and level3) for binary, left associative
    ///    operators. By default, level1 handles addition and subtraction (+,-), level2
    ///    handles multiplicative operands (*, /, %, \) and level three handles exponents
    ///    (^). Adding a new operator at any of these levels requires 2 steps. One is to
    ///    modify the init_operators function to include a symbol for the new operator,
    ///    specifying the precedence level and the character denoting the operation. Only
    ///    single character operators can be added without additional changes to the
    ///    lexical scanner. The second step is to modify the calc_op function to handle the
    ///    operation, which should become clear once in the code. Level4 handles right
    ///    associative unary operators (-, + i.e. negation, etc.) and level5 handles left
    ///    associative unary operators (! factorials). The process to add new operators at
    ///    these levels is the same as above.
    ///
    ///    The addition of functions is equally simple. The new function name must first be
    ///    added to the m_funcs array which is initialized in the declarations of the
    ///    mcCalc class. Then the calc_function function must be modified to perform the
    ///    function operation. Function arguments are passed to the calc_function function
    ///    in a collection. The parser simple passes in the number of comma delimited
    ///    values it finds enclosed in parenthesis, following the function. The
    ///    calc_function function is responsible for removing the number of arguments
    ///    required for the function, and generating any errors when an incorrect number of
    ///    arguments is passed. Variable length argument lists can even be implemented by
    ///    simply indicating the number of function arguments in the first argument. Points
    ///    of interest
    ///
    ///    There are several interesting modifications to this code that could provide
    ///    additional utility. Variable identifiers and substitution could also be of use
    ///    to those needing a more thorough evaluation tool. Support for caller defined
    ///    functions or operators through the use of delegates would be a nice addition for
    ///    anyone interested in using this code as an external assembly. There are
    ///    certainly more, and any suggestions or modifications for such are welcome.
    ///    Hopefully this code will prove useful to you in it's application or at least in
    ///    it's explanation of some of the principles behind expression evaluation.
    ///
    ///    Translated from VB.NET by Rob Siklos
    ///
    ///    No copyright or license specified - using as public domain code
    /// </remarks>
    /// <see>http://www.codeproject.com/vb/net/math_expression_evaluator.asp</see>
    public class Calculation
    {
        public Calculation()
        {
            init();
        }

        private class mcSymbol : System.Collections.Generic.IComparer<mcSymbol>
        {
            public string Token;
            public Calculation.TOKENCLASS Cls;
            public PRECEDENCE PrecedenceLevel;
            public string tag;

            public delegate int compare_function(object x, object y);

            public virtual int Compare(mcSymbol asym, mcSymbol bsym)
            {
                if (string.Compare(asym.Token, bsym.Token) != 0)
                {
                    return string.CompareOrdinal(asym.Token, bsym.Token);
                }

                if ((int)asym.PrecedenceLevel == -1 || (int)bsym.PrecedenceLevel == -1)
                {
                    return 0;
                }
                if (asym.PrecedenceLevel > bsym.PrecedenceLevel)
                {
                    return 1;
                }
                if (asym.PrecedenceLevel < bsym.PrecedenceLevel)
                {
                    return -1;
                }
                return 0;
            }
        }
        private enum PRECEDENCE
        {
            NONE = 0,
            LEVEL0 = 1,
            LEVEL1 = 2,
            LEVEL2 = 3,
            LEVEL3 = 4,
            LEVEL4 = 5,
            LEVEL5 = 6
        }

        private enum TOKENCLASS
        {
            KEYWORD = 1,
            IDENTIFIER = 2,
            NUMBER = 3,
            OPERATOR = 4,
            PUNCTUATION = 5
        }

        private int[,] m_State;
        private string m_colstring;
        private const string ALPHA = "_ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        private const string DIGITS = "#0123456789";

        private string[] m_funcs = { "sin", "cos", "tan",
                                     "asin", "acos", "atan",
                                     "sqrt", "max", "min",
                                     "floor", "ceil",
                                     "log", "log10", "ln",
                                     "round", "abs", "neg", "pos",
                                     "pow", "pi",
                                     "manningflow", "coalesce", "null",
                                     "ellipse_area","pipe_area","fastflow",
                                     "eq", "ne", "ge", "gt", "lt", "le",
                                     "if",
                                   };

        private List<mcSymbol> m_operators;

        private void init_operators()
        {
            mcSymbol op;
            m_operators = new List<mcSymbol>();

            op = new mcSymbol();
            op.Token = "-";
            op.Cls = TOKENCLASS.OPERATOR;
            op.PrecedenceLevel = PRECEDENCE.LEVEL1;
            m_operators.Add(op);

            op = new mcSymbol();
            op.Token = "+";
            op.Cls = TOKENCLASS.OPERATOR;
            op.PrecedenceLevel = PRECEDENCE.LEVEL1;
            m_operators.Add(op);

            op = new mcSymbol();
            op.Token = "*";
            op.Cls = TOKENCLASS.OPERATOR;
            op.PrecedenceLevel = PRECEDENCE.LEVEL2;
            m_operators.Add(op);

            op = new mcSymbol();
            op.Token = "/";
            op.Cls = TOKENCLASS.OPERATOR;
            op.PrecedenceLevel = PRECEDENCE.LEVEL2;
            m_operators.Add(op);

            op = new mcSymbol();
            op.Token = "\\";
            op.Cls = TOKENCLASS.OPERATOR;
            op.PrecedenceLevel = PRECEDENCE.LEVEL2;
            m_operators.Add(op);

            op = new mcSymbol();
            op.Token = "%";
            op.Cls = TOKENCLASS.OPERATOR;
            op.PrecedenceLevel = PRECEDENCE.LEVEL2;
            m_operators.Add(op);

            op = new mcSymbol();
            op.Token = "^";
            op.Cls = TOKENCLASS.OPERATOR;
            op.PrecedenceLevel = PRECEDENCE.LEVEL3;
            m_operators.Add(op);

            op = new mcSymbol();
            op.Token = "!";
            op.Cls = TOKENCLASS.OPERATOR;
            op.PrecedenceLevel = PRECEDENCE.LEVEL5;
            m_operators.Add(op);

            op = new mcSymbol();
            op.Token = "&";
            op.Cls = TOKENCLASS.OPERATOR;
            op.PrecedenceLevel = PRECEDENCE.LEVEL5;
            m_operators.Add(op);

            op = new mcSymbol();
            op.Token = "-";
            op.Cls = TOKENCLASS.OPERATOR;
            op.PrecedenceLevel = PRECEDENCE.LEVEL4;
            m_operators.Add(op);

            op = new mcSymbol();
            op.Token = "+";
            op.Cls = TOKENCLASS.OPERATOR;
            op.PrecedenceLevel = PRECEDENCE.LEVEL4;
            m_operators.Add(op);

            op = new mcSymbol();
            op.Token = "(";
            op.Cls = TOKENCLASS.OPERATOR;
            op.PrecedenceLevel = PRECEDENCE.LEVEL5;
            m_operators.Add(op);

            op = new mcSymbol();
            op.Token = ")";
            op.Cls = TOKENCLASS.OPERATOR;
            op.PrecedenceLevel = PRECEDENCE.LEVEL0;
            m_operators.Add(op);

            m_operators.Sort(op);
        }

        public double? evaluate(string expression)
        {
            Queue symbols = null;

            if (AxisUtil.IsNumeric(expression))
            {
                return (Double.Parse(expression));
            }
            if (AxisUtil.substr_count(expression, "(") != AxisUtil.substr_count(expression, ")"))
            {
                throw new Exception("Bracket mismatch");
            }

            // skip regex stuff
            if ((AxisUtil.substr_count(expression, "(") == 3) && (expression.Contains("fast_flow")))
            {
                return 0;
            }
            else
            {
                calc_scan(expression, ref symbols);
                return level0(ref symbols);
            }
        }

        private double? calc_op(mcSymbol op, double? operand1, double? operand2)
        {
            if (op.Token.ToLower() == "&")
            {
                // sample to show addition of custom operator
                return 5;
            }
            else if (op.Token.ToLower() == "^")
            {
                if (operand1 == null || operand2 == null)
                {
                    return null;
                }
                return (System.Math.Pow(operand1.Value, operand2.Value));
            }
            else if (op.Token.ToLower() == "+")
            {
                if (op.PrecedenceLevel == PRECEDENCE.LEVEL1)
                {
                    return (operand2 + operand1);
                }
                else if (op.PrecedenceLevel == PRECEDENCE.LEVEL4)
                {
                    return operand1;
                }
                throw new Exception("Unexpected execution point");
            }
            else if (op.Token.ToLower() == "-")
            {
                if (op.PrecedenceLevel == PRECEDENCE.LEVEL1)
                {
                    return (operand1 - operand2);
                }
                else if (op.PrecedenceLevel == PRECEDENCE.LEVEL4)
                {
                    return -1 * operand1;
                }
                throw new Exception("Unexpected execution point");
            }
            else if (op.Token.ToLower() == "*")
            {
                return (operand2 * operand1);
            }
            else if (op.Token.ToLower() == "/")
            {
                return (operand1 / operand2);
            }
            else if (op.Token.ToLower() == "\\")
            {
                if (operand1 == null || operand2 == null)
                {
                    return null;
                }
                return (Convert.ToInt64(operand1.Value) / Convert.ToInt64(operand2.Value));
            }
            else if (op.Token.ToLower() == "%")
            {
                return (operand1 % operand2);
            }
            else if (op.Token.ToLower() == "!")
            {
                if (operand1 == null)
                {
                    return null;
                }
                double res = 1;
                if (operand1 > 1)
                {
                    for (int i = Convert.ToInt32(operand1.Value); i >= 1; i--)
                    {
                        res *= i;
                    }
                }
                return (res);
            }
            else
            {
                throw new Exception("Unknown Operator: " + op.Token);
            }
        }

        private double? calc_function(string func, List<double?> args)
        {
            string funcLC = func.ToLower();

            // special null consideration for functions that can take null arguments
            if (funcLC == "coalesce")
            {
                // return the first non-null argument, or null if none exist
                foreach (double? arg in args)
                {
                    if (arg.HasValue)
                    {
                        return arg;
                    }
                }
                return null;
            }

            // COMPARISON FUNCTIONS
            else if (funcLC == "eq")
            {
                if (!args[0].HasValue) return args[1].HasValue ? 0 : 1;
                else if (!args[1].HasValue) return args[0].HasValue ? 0 : 1;
                else return (args[0].Value == args[1].Value) ? 1 : 0;
            }
            else if (funcLC == "ne")
            {
                if (!args[0].HasValue) return args[1].HasValue ? 1 : 0;
                else if (!args[1].HasValue) return args[0].HasValue ? 1 : 0;
                else return (args[0].Value != args[1].Value) ? 1 : 0;
            }

            // CONDITIONAL FUNCTIONS
            else if (funcLC == "if")
            {
                // syntax: if(a, b, c)
                // => if (a != 0), return b, else return c
                if (!args[0].HasValue) return null;
                else if (args[0].Value == 0)
                    if (args[2].HasValue) return args[2].Value;
                    else return null;
                else
                    if (args[1].HasValue) return args[1].Value;
                else return null;
            }

            // make sure that all the arguments are non-null
            foreach (double? arg in args)
            {
                if (arg == null)
                {
                    return null;
                }
            }

            if (funcLC == "null")
            {
                return null;
            }
            else if (funcLC == "pi")
            {
                return System.Math.PI;
            }
            else if (funcLC == "cos")
            {
                return (System.Math.Cos(args[0].Value));
            }
            else if (funcLC == "sin")
            {
                return (System.Math.Sin(args[0].Value));
            }
            else if (funcLC == "tan")
            {
                return (System.Math.Tan(args[0].Value));
            }
            else if (funcLC == "floor")
            {
                return (System.Math.Floor(args[0].Value));
            }
            else if (funcLC == "ceil")
            {
                return (System.Math.Ceiling(args[0].Value));
            }
            else if (funcLC == "max")
            {
                return (System.Math.Max(args[0].Value, args[1].Value));
            }
            else if (funcLC == "min")
            {
                return (System.Math.Min(args[0].Value, args[1].Value));
            }
            else if (funcLC == "asin")
            {
                return (System.Math.Asin(args[0].Value));
            }
            else if (funcLC == "acos")
            {
                return (System.Math.Acos(args[0].Value));
            }
            else if (funcLC == "atan")
            {
                return (System.Math.Atan(args[0].Value));
            }
            else if (funcLC == "sqrt")
            {
                return (System.Math.Sqrt(args[0].Value));
            }
            else if (funcLC == "log")
            {
                return (System.Math.Log(args[0].Value));
            }
            else if (funcLC == "log10")
            {
                return (System.Math.Log10(args[0].Value));
            }
            else if (funcLC == "abs")
            {
                return (System.Math.Abs(args[0].Value));
            }
            else if (funcLC == "round")
            {
                return (System.Math.Round(args[0].Value));
            }
            else if (funcLC == "ln")
            {
                return (System.Math.Log(args[0].Value));
            }
            else if (funcLC == "neg")
            {
                return (-1 * args[0].Value);
            }
            else if (funcLC == "pos")
            {
                return (+1 * args[0].Value);
            }
            else if (funcLC == "pow")
            {
                return (System.Math.Pow(args[0].Value, args[1].Value));
            }
            else if (funcLC == "ellipse_area")
            {
                double x_rad = args[0].Value / 2;
                double y_rad = args[1].Value / 2;
                double level_water = args[2].Value;

                return get_ellipse_area(x_rad, y_rad, level_water);
            }
            else if (funcLC == "pipe_area")
            {
                double x_rad = args[0].Value / 2;
                double y_rad = args[1].Value / 2;
                double level_water = args[2].Value;
                double level_silt = args[3].Value;

                if (level_silt >= level_water)
                {
                    return 0;
                }
                else
                {
                    return get_ellipse_area(x_rad, y_rad, level_water) - get_ellipse_area(x_rad, y_rad, level_silt);
                }
            }
            /*
        else if (funcLC == "fastflow")
        {
            double rad = args[0].Value / 2;
            double level = args[1].Value;
            double velocity = args[2].Value;

            double constA = 1 / rad / 2.18;
            double constB = -0.0194 * rad + 1.3958;
            double constC = 0.6429 * rad;

            return (constA * Math.Pow(level, 3) + constB * Math.Pow(level, 2) + constC * level) * velocity * 1000;
        }
             */
            else if (funcLC == "manningflow")
            {
                double rad = args[0].Value / 2;
                double slope = args[1].Value;
                double rough = args[2].Value;
                double depth = args[3].Value;

                double flow_area = get_ellipse_area(rad, rad, depth);

                if (depth > 2 * rad)
                {
                    return 0;
                }
                else if (depth < rad)
                {
                    double wetted_p = (rad * (2 * System.Math.Acos((rad - depth) / rad)));
                    return (1 / rough) * flow_area * System.Math.Pow((flow_area / wetted_p), 0.6666D) * System.Math.Pow(slope, 0.5D) * 1000;
                }
                else
                {
                    double wetted_p = (System.Math.PI * rad) + (rad * (2 * System.Math.Acos((rad - depth) / rad)));
                    return (1 / rough) * flow_area * System.Math.Pow((flow_area / wetted_p), 0.6666D) * System.Math.Pow(slope, 0.5D) * 1000;
                }
            }

            // COMPARISON FUNCTIONS
            else if (funcLC == "ge")
            {
                return (args[0].Value >= args[1].Value) ? 1 : 0;
            }
            else if (funcLC == "gt")
            {
                return (args[0].Value > args[1].Value) ? 1 : 0;
            }
            else if (funcLC == "lt")
            {
                return (args[0].Value < args[1].Value) ? 1 : 0;
            }
            else if (funcLC == "le")
            {
                return (args[0].Value <= args[1].Value) ? 1 : 0;
            }

            throw new Exception("Unexpected Execution Point: calc_function: func=" + func);
        }

        private double get_ellipse_area(double x_rad, double y_rad, double y_level)
        {
            double level;
            level = y_level;
            level = System.Math.Min(level, 2 * y_rad);
            level = System.Math.Max(level, 0);
            double total_area = System.Math.PI * x_rad * y_rad;
            double d = y_rad - level;
            double upperarea = y_rad * x_rad * (System.Math.PI / 2 + System.Math.Asin(d / y_rad))
                               + x_rad * d * System.Math.Sqrt(y_rad * y_rad - d * d) / y_rad;
            return total_area - upperarea;
        }

        private double identifier(string token)
        {
            if (token.ToLower() == "e")
            {
                return System.Math.E;
            }
            else if (token.ToLower() == "pi")
            {
                return System.Math.PI;
            }
            else
            {
                throw new Exception("Invalid keyword: " + token);
            }
        }

        private bool is_operator(string token, PRECEDENCE level, ref mcSymbol operator2)
        {
            try
            {
                mcSymbol op = new mcSymbol();
                op.Token = token;
                op.PrecedenceLevel = level;
                op.tag = "test";
                int ir = m_operators.BinarySearch(op, op);
                if (ir > -1)
                {
                    operator2 = ((mcSymbol)(m_operators[ir]));
                    return true;
                }
                return false;
            }
            catch
            {
                return false;
            }
        }

        private bool is_function(string token)
        {
            try
            {
                int lr = Array.BinarySearch(m_funcs, token.ToLower());
                return (lr > -1);
            }
            catch
            {
                return false;
            }
        }

        public bool calc_scan(string line, ref Queue symbols)
        {
            int sp;
            int cp;
            int col;
            int lex_state;
            char cc;
            symbols = new Queue();
            line = line + " ";
            sp = 0;
            cp = 0;
            lex_state = 1;
            while (cp <= line.Length - 1)
            {
                cc = line[cp];
                col = m_colstring.IndexOf(cc) + 3;
                if (col == 2)
                {
                    if (ALPHA.IndexOf(char.ToUpper(cc)) >= 0)
                    {
                        col = 1;
                    }
                    else if (DIGITS.IndexOf(char.ToUpper(cc)) >= 0)
                    {
                        col = 2;
                    }
                    else
                    {
                        col = 6;
                    }
                }
                else if (col > 5)
                {
                    col = 7;
                }
                lex_state = m_State[lex_state - 1, col - 1];
                if (lex_state == 3)
                {
                    mcSymbol sym = new mcSymbol();
                    sym.Token = line.Substring(sp, cp - sp);
                    if (is_function(sym.Token))
                    {
                        sym.Cls = TOKENCLASS.KEYWORD;
                    }
                    else
                    {
                        sym.Cls = TOKENCLASS.IDENTIFIER;
                    }
                    symbols.Enqueue(sym);
                    lex_state = 1;
                    cp = cp - 1;
                }
                else if (lex_state == 5)
                {
                    mcSymbol sym = new mcSymbol();
                    sym.Token = line.Substring(sp, cp - sp);
                    sym.Cls = TOKENCLASS.NUMBER;
                    symbols.Enqueue(sym);
                    lex_state = 1;
                    cp = cp - 1;
                }
                else if (lex_state == 6)
                {
                    mcSymbol sym = new mcSymbol();
                    sym.Token = line.Substring(sp, cp - sp + 1);
                    sym.Cls = TOKENCLASS.PUNCTUATION;
                    symbols.Enqueue(sym);
                    lex_state = 1;
                }
                else if (lex_state == 7)
                {
                    mcSymbol sym = new mcSymbol();
                    sym.Token = line.Substring(sp, cp - sp + 1);
                    sym.Cls = TOKENCLASS.OPERATOR;
                    symbols.Enqueue(sym);
                    lex_state = 1;
                }
                cp += 1;
                if (lex_state == 1)
                {
                    sp = cp;
                }
            }
            return true;
        }

        private void init()
        {
            int[,] state = { { 2, 4, 1, 1, 4, 6, 7 },
                             { 2, 3, 3, 3, 3, 3, 3 },
                             { 1, 1, 1, 1, 1, 1, 1 },
                             { 2, 4, 5, 5, 4, 5, 5 },
                             { 1, 1, 1, 1, 1, 1, 1 },
                             { 1, 1, 1, 1, 1, 1, 1 },
                             { 1, 1, 1, 1, 1, 1, 1 } };
            init_operators();
            m_State = state;
            m_colstring = Convert.ToChar(9) + " " + ".()";
            foreach (mcSymbol op in m_operators)
            {
                m_colstring = m_colstring + op.Token;
            }
            Array.Sort(m_funcs);
        }


        #region "Recursive Descent Parsing Functions"

        private double? level0(ref Queue tokens)
        {
            var res =  level1(ref tokens);
            return res;
        }

        private double? level1_prime(ref Queue tokens, double? result)
        {
            mcSymbol symbol;
            mcSymbol operator2;
            operator2 = null;
            if (tokens.Count > 0)
            {
                symbol = ((mcSymbol)(tokens.Peek()));
            }
            else
            {
                return result;
            }
            if (is_operator(symbol.Token, PRECEDENCE.LEVEL1, ref operator2))
            {
                tokens.Dequeue();
                result = calc_op(operator2, result, level2(ref tokens));
                result = level1_prime(ref tokens, result);
            }
            return result;
        }

        private double? level1(ref Queue tokens)
        {
            var res = level1_prime(ref tokens, level2(ref tokens));

            return res;
        }

        private double? level2(ref Queue tokens)
        {
            var res = level2_prime(ref tokens, level3(ref tokens));

            return res;
        }

        private double? level2_prime(ref Queue tokens, double? result)
        {
            mcSymbol symbol;
            mcSymbol operator2;
            operator2 = null;
            if (tokens.Count > 0)
            {
                symbol = ((mcSymbol)(tokens.Peek()));
            }
            else
            {
                return result;
            }
            if (is_operator(symbol.Token, PRECEDENCE.LEVEL2, ref operator2))
            {
                tokens.Dequeue();
                result = calc_op(operator2, result, level3(ref tokens));
                result = level2_prime(ref tokens, result);
            }
            return result;
        }

        private double? level3(ref Queue tokens)
        {
            var res =  level3_prime(ref tokens, level4(ref tokens));
            return res;
        }

        private double? level3_prime(ref Queue tokens, double? result)
        {
            mcSymbol symbol;
            mcSymbol operator2;
            operator2 = null;
            if (tokens.Count > 0)
            {
                symbol = ((mcSymbol)(tokens.Peek()));
            }
            else
            {
                return result;
            }
            if (is_operator(symbol.Token, PRECEDENCE.LEVEL3, ref operator2))
            {
                tokens.Dequeue();
                result = calc_op(operator2, result, level4(ref tokens));
                result = level3_prime(ref tokens, result);
            }
            return result;
        }

        private double? level4(ref Queue tokens)
        {
            var res = level4_prime(ref tokens);

            return res;
        }

        private double? level4_prime(ref Queue tokens)
        {
            mcSymbol symbol;
            mcSymbol operator2;
            operator2 = null;
            if (tokens.Count > 0)
            {
                symbol = ((mcSymbol)(tokens.Peek()));
            }
            else
            {
                throw new System.Exception("Invalid expression.");
            }
            if (is_operator(symbol.Token, PRECEDENCE.LEVEL4, ref operator2))
            {
                tokens.Dequeue();
                return calc_op(operator2, level5(tokens), 0);
            }
            else
            {
                return level5(tokens);
            }
        }

        private double? level5(Queue tokens)
        {
            var res =  level5_prime(tokens, level6(ref tokens));

            return res;
        }

        private double? level5_prime(Queue tokens, double? result)
        {
            mcSymbol symbol;
            mcSymbol operator2;
            operator2 = null;
            if (tokens.Count > 0)
            {
                symbol = ((mcSymbol)(tokens.Peek()));
            }
            else
            {
                return result;
            }
            if (is_operator(symbol.Token, PRECEDENCE.LEVEL5, ref operator2))
            {
                tokens.Dequeue();
                return calc_op(operator2, result, 0);
            }
            else
            {
                return result;
            }
        }

        private double? level6(ref Queue tokens)
        {
            mcSymbol symbol;
            if (tokens.Count > 0)
            {
                symbol = ((mcSymbol)(tokens.Peek()));
            }
            else
            {
                throw new System.Exception("Invalid expression.");
            }
            double? val;
            if (symbol.Token == "(")
            {
                tokens.Dequeue();
                val = level0(ref tokens);
                symbol = ((mcSymbol)(tokens.Dequeue()));
                if (symbol.Token != ")")
                {
                    throw new System.Exception("Invalid expression.");
                }
                return val;
            }
            else
            {
                if (symbol.Cls == TOKENCLASS.IDENTIFIER)
                {
                    tokens.Dequeue();
                    return identifier(symbol.Token);
                }
                else if (symbol.Cls == TOKENCLASS.KEYWORD || symbol.Token == ")")
                {
                    tokens.Dequeue();
                    return calc_function(symbol.Token, arguments(tokens));
                }
                else if (symbol.Cls == TOKENCLASS.NUMBER)
                {
                    tokens.Dequeue();
                    return Convert.ToDouble(symbol.Token);
                }
                else
                {
                    throw new System.Exception("Invalid expression.");
                }
            }
        }

        private List<double?> arguments(Queue tokens)
        {
            mcSymbol symbol;
            List<double?> args = new List<double?>();
            if (tokens.Count > 0)
            {
                symbol = ((mcSymbol)(tokens.Peek()));
            }
            else
            {
                throw new System.Exception("Invalid expression.");
            }
            if (symbol.Token == "(")
            {
                tokens.Dequeue();
                symbol = (mcSymbol)(tokens.Peek());
                if (symbol.Token != ")")
                {
                    // function has params
                    args.Add(level0(ref tokens));
                }
                symbol = ((mcSymbol)(tokens.Dequeue()));
                while (symbol.Token != ")")
                {
                    if (symbol.Token == ",")
                    {
                        args.Add(level0(ref tokens));
                    }
                    else
                    {
                        throw new System.Exception("Invalid expression - expected ','");
                    }
                    symbol = ((mcSymbol)(tokens.Dequeue()));
                }
                return args;
            }
            else
            {
                throw new System.Exception("Invalid expression.");
            }
        }
    }

    #endregion "Recursive Descent Parsing Functions"
}
