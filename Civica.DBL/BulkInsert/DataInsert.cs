﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using Civica.Domain.CivicaDomain;
namespace Civica.DBL
{
    class DataInsert
    {
        public void transferData(Civica.Domain.CivicaDomain.datacurrentEntities Context)
        {

            SqlConnection dbConnection = Context.Database.Connection as SqlConnection;
            DataTable table = new DataTable();
            table.Columns.Add("MonitoringSensorId", typeof(int));
            table.Columns.Add("Value", typeof(double));
            table.Columns.Add("TimeStamp", typeof(DateTimeOffset));
            table.Columns.Add("Processed", typeof(int));

            dbConnection.Open();
            // Instantiate SqlBulkCopy and configure Bulkcopy propoerties
            var transaction = dbConnection.BeginTransaction();
            using (SqlBulkCopy s = new SqlBulkCopy(dbConnection, SqlBulkCopyOptions.TableLock, transaction))
            {
                s.BatchSize = 2000;
                s.NotifyAfter = 2000;
                s.BulkCopyTimeout = 18000;
                s.DestinationTableName = "MonitoringData";

                s.ColumnMappings.Add("MonitoringSensorId", "MonitoringSensorId"); s.ColumnMappings.Add("Value", "Value"); s.ColumnMappings.Add("TimeStamp", "TimeStamp");
                s.ColumnMappings.Add("Processed", "Processed");
                s.WriteToServer(table);
                transaction.Commit();
                s.ColumnMappings.Clear();
            }
            dbConnection.Close();
            dbConnection.Dispose();
        }
    }
}
