﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using Civica.Domain.CivicaDomain;
using Civica.Infrastructure.BaseClasses;

namespace Civica.DBL.DBClasses
{
    public class StationDBMethods
    {

        public static MonitoringStation GetStationByUniqueId(string uniqueId)
        {

            var ccf = new datacurrentEntities();

            var station = ccf.MonitoringStations.AsQueryable().Where(x => x.Active == true && x.StateId == (int)BaseEnum.StationState.Receiving && x.MonitoringLogger.UniqueId == uniqueId && x.MonitoringLogger.Active == true && x.MaintanaceMode == false).FirstOrDefault();

            return station;
        }


        public static MonitoringStation GetStationByUniqueId(string uniqueId, BaseEnum.LoggerType type)
        {

            var ccf = new datacurrentEntities();

            var station = ccf.MonitoringStations.AsQueryable().Where(x => x.Active == true && x.StateId == (int)BaseEnum.StationState.Receiving && x.MonitoringLogger.UniqueId == uniqueId && x.MaintanaceMode == false).FirstOrDefault();

            return station;
        }

        public static MonitoringStation GetStationById(int Id)
        {
            var ccf = new datacurrentEntities();
            var station = ccf.MonitoringStations.Where(x => x.Id == Id && x.MaintanaceMode == false && x.StateId == (int)BaseEnum.StationState.Receiving && x.Active == true).FirstOrDefault();
            return station;
        }

        public static MonitoringStation GetInactiveStationByStationById(int Id)
        {
            var ccf = new datacurrentEntities();
            var station = ccf.MonitoringStations.Where(x => x.Id == Id  && x.Active == true).FirstOrDefault();
            return station;
        }


        public static List<MonitoringStation> GetListOfStationsByLoggerType(int loggerTypeId)  
        {
            var ccf = new datacurrentEntities();

            var station = ccf.MonitoringStations.Where(x => x.MonitoringLogger.LoggerTypeId ==  loggerTypeId && x.Active == true && x.MaintanaceMode == false && x.StateId == (int)BaseEnum.StationState.Receiving).ToList();

            return station;
        }

    }
}
