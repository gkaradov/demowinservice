﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Civica.Domain.CivicaDomain;
using Civica.DBL.SPModels;
using System.Data;
using System.Data.SqlClient;
using Civica.Infrastructure.BaseClasses;
using Civica.Domain.TelogDomain;

namespace Civica.DBL.DBClasses
{
    public class ReactorDbMethods
    {

        public static ServiceResult<bool> UpdateReactorBEvent(ModemData data)
        {
            ServiceResult<bool> res = new ServiceResult<bool>(true);
            var ccf = new datacurrentEntities();
            var log = new MonitoringDataReactorBlueTreeLog();
            var message = String.Empty;
            try
            {
                log.IMEI = data.IMEI;
                log.MessageTypeId = 1;

                if (data.DI1 == 1)
                {
                    log.DI1 = true;
                }
                else
                {
                    log.DI1 = false;
                }

                if (data.DI2 == 1)
                {
                    log.DI2 = true;
                }
                else
                {
                    log.DI2 = false;
                }

                if (data.DI3 == 1)
                {
                    log.DI3 = true;
                }
                else
                {
                    log.DI3 = false;
                }

                if (data.DI4 == 1)
                {
                    log.DI4 = true;
                }
                else
                {
                    log.DI4 = false;
                }

                log.AI1 = (decimal)data.AI1;
                log.AI2 = (decimal)data.AI2;
                log.AI3 = (decimal)data.AI3;
                log.Power = (decimal)data.Power;
                log.TimeStamp = DateTimeOffset.Now.ToUniversalTime();

                var station = StationDBMethods.GetStationByUniqueId(data.IMEI.ToString());

                if (station != null)
                {
                    if (station.MonitoringSensors != null)
                    {
                        if (station.MonitoringSensors.Count() > 0)
                        {

                            foreach (var sensor in station.MonitoringSensors)
                            {
                                var createRecord = true;

                                decimal dvalue = 0;

                                if (sensor.MonitoringSensorEquations != null)
                                {

                                    var currentequation = new MonitoringSensorEquation();

                                    var fe = sensor.MonitoringSensorEquations.Where(p => p.ValidUntil == null).Where(x => x.Active == true).FirstOrDefault();


                                    var ce = sensor.MonitoringSensorEquations.Where(x => x.Active == true).Where(x => x.ValidUntil != null && DateTimeOffset.Parse(x.ValidUntil.ToString()).UtcDateTime >= DateTimeOffset.UtcNow).OrderBy(x => x.ValidUntil).FirstOrDefault();

                                    if (ce != null)
                                    {
                                        currentequation = ce;
                                    }
                                    else
                                    {
                                        if (fe != null)
                                        {
                                            currentequation = fe;
                                        }
                                    }


                                    switch (currentequation.MonitoringLoggerChannel.Channel)
                                    {
                                        case BaseEnum.BlueTreeChanels.AI1:
                                            dvalue = (decimal)log.AI1;
                                            break;
                                        case BaseEnum.BlueTreeChanels.AI2:
                                            dvalue = (decimal)log.AI2;
                                            break;
                                        case BaseEnum.BlueTreeChanels.AI3:
                                            dvalue = (decimal)log.AI3;
                                            break;

                                        case BaseEnum.BlueTreeChanels.DI1:
                                            if (log.DI1 == false)
                                            {
                                                createRecord = false;
                                                dvalue = 0;
                                            }
                                            else
                                            {
                                                dvalue = 1;
                                            }
                                            break;
                                        case BaseEnum.BlueTreeChanels.DI2:
                                            if (log.DI2 == false)
                                            {
                                                createRecord = false;
                                                dvalue = 0;
                                            }
                                            else
                                            {
                                                dvalue = 1;
                                            }
                                            break;
                                        case BaseEnum.BlueTreeChanels.DI3:
                                            if (log.DI3 == false)
                                            {
                                                createRecord = false;
                                                dvalue = 0;
                                            }
                                            else
                                            {
                                                dvalue = 1;
                                            }
                                            break;
                                        case BaseEnum.BlueTreeChanels.DI4:
                                            if (log.DI4 == false)
                                            {
                                                createRecord = false;
                                                dvalue = 0;
                                            }
                                            else
                                            {
                                                dvalue = 1;
                                            }
                                            break;

                                        case BaseEnum.BlueTreeChanels.PWR:
                                            dvalue = (decimal)log.Power;
                                            break;
                                    }

                                    if (createRecord)
                                    {
                                        var sdata = new MonitoringData();

                                        var sdaraRaw = new MonitoringDataRaw();

                                        sdata.MonitoringSensorId = sensor.Id;
                                        sdata.Processed = 0;
                                        sdata.Value = dvalue;
                                        sdata.TimeStamp = DateTimeOffset.Now.ToUniversalTime();
                                        ccf.MonitoringDatas.Add(sdata);

                                        sdaraRaw.MonitoringSensorId = sensor.Id;
                                        sdaraRaw.Value = dvalue;
                                        sdaraRaw.Timestamp = DateTimeOffset.Now.ToUniversalTime();
                                        ccf.MonitoringDataRaws.Add(sdaraRaw);

                                        ccf.SaveChanges();
                                    }
                                }
                                else
                                {
                                    message = message + " / Warning no equation found for sensor ID: " + sensor.Id;

                                }
                            }
                        }
                        else
                        {
                            message = "Warning : No sensors set for the station ID: " + station.Id;
                        }
                    }
                    else
                    {
                        message = "Warning : No sensors set for the station ID: " + station.Id;
                    }

                    log.Comment = message;
                    log.Processed = true;
                    ccf.MonitoringDataReactorBlueTreeLogs.Add(log);
                    ccf.SaveChanges();
                }
                else
                {
                    log.Comment = "Warning: No station with Unique Loger ID :" + data.IMEI.ToString() + " found";
                    log.Processed = false;
                    ccf.MonitoringDataReactorBlueTreeLogs.Add(log);
                    ccf.SaveChanges();
                }

            }
            catch (Exception e)
            {
                log.Comment = "Error: " + e.Message;
                log.Processed = false;
                ccf.MonitoringDataReactorBlueTreeLogs.Add(log);
                ccf.SaveChanges();

            }

            return res;
        }

        public static ServiceResult<bool> UpdateReactorIP(ModemInfo data)
        {
            ServiceResult<bool> res = new ServiceResult<bool>(true);
            var ccf = new datacurrentEntities();
            var log = new MonitoringDataReactorBlueTreeLog();

            try
            {
                var station = StationDBMethods.GetStationByUniqueId(data.IMEI.ToString());

                if (station != null)
                {

                    var sl = ccf.MonitoringLoggers.Where(x => x.Id == station.MonitoringLoggerId).FirstOrDefault();
                    if (sl != null)
                    {
                        sl.IPAddress = data.ip_addr.ToString();
                        sl.LastTimeIPUpdated = DateTimeOffset.Now.ToUniversalTime();
                    }
                    
                    log.Processed = true;
                }
                else
                {
                    log.Comment = "Warning: No station with Unique Loger ID :" + data.IMEI.ToString() + " found";
                    log.Processed = false;
                }

                log.IMEI = data.IMEI;
                log.MessageTypeId = 2;
                log.IP = data.ip_addr.ToString();
                log.TimeStamp = DateTimeOffset.Now.ToUniversalTime();
                ccf.MonitoringDataReactorBlueTreeLogs.Add(log);
                ccf.SaveChanges();


            }
            catch(Exception e)
            {
            }

                return res;
        }

    }
}
