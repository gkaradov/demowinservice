﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Civica.DBL.SPModels
{
    public class SensorDataSP
    {
        public int SensorId { get; set; }
        public decimal Value { get; set; }
        public DateTimeOffset Timestamp { get; set; }
    }

    public class SensorDataWithReplacmentStringSP
    {
        public int SensorId { get; set; }
        public decimal Value { get; set; }
        public DateTimeOffset Timestamp { get; set; }
        public string ReplacmentString { get; set; }
    }
}
