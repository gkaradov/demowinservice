﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Civica.Domain.CivicaDomain;
using Civica.DBL.SPModels;
using System.Data;
using Civica.Infrastructure.BaseClasses;

namespace Civica.DBL.DBClasses
{

    public class ImportLog
    {
        public static void AddLogEntry(string importType, string messageType, int stationId, string fileName, string uniqueId, string errorMEssage, string stack, DateTimeOffset timestamp)
        {

            try
            {
                var ccf = new datacurrentEntities();
                var newlog = new MonitoringDataImportLog();

                newlog.MessageType = messageType;
                newlog.ImportType = importType;
                newlog.StationId = stationId;
                newlog.FileMane = fileName;
                newlog.UniqueId = uniqueId;
                newlog.ErrorMessage = errorMEssage;
                newlog.Stack = stack;
                newlog.TimeStamp = timestamp;

                ccf.MonitoringDataImportLogs.Add(newlog);
                ccf.SaveChanges();

            }
            catch (Exception e)
            {
               
            }



        }

    }

}