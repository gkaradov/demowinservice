﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Civica.DBL.DBClasses
{
    public class DelayedSensorWithOffset
    {
        public int Id { get; set; }
        public double Offset { get; set; }
        public string ReplaceString { get; set; }
    }
}
