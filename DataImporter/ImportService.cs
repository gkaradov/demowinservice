﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.InteropServices;
using Civica.Infrastructure.BaseClasses;
using System.Configuration;
using Civica.BLL.ServiceClasses.DataImporter.Interfaces;
using Civica.BLL.ServiceClasses.DataImporter;
using Microsoft.Practices.Unity;
using NLog;

namespace DataImporter
{
    partial class ImportService : ServiceBase
    {

        public DCLogger taskLogger = new DCLogger();
        public DCLogger telogLogger = new DCLogger();

        public DCLogger logger = new DCLogger();

        private Task MonitoringImportTask;
        private Task DetectronicDirectory;
        private Task DetectronicServer;
        private Task IsodaqXMLPath;
        private Task XRSPath;
        private Task ADSFTP;
        private Task ADSProfile;
        private Task TelogDB;
        private Task TelogPath;
        private Task FLWKMap;
        private Task HOBOUX120Task;
        private Task RemoteSense;

        // private readonly Services.Engine.AuctionControl _auctionControl;


        [StructLayout(LayoutKind.Sequential)]
        public struct ServiceStatus
        {
            public long dwServiceType;
            public BaseEnum.ServiceState dwCurrentState;
            public long dwControlsAccepted;
            public long dwWin32ExitCode;
            public long dwServiceSpecificExitCode;
            public long dwCheckPoint;
            public long dwWaitHint;
        };

        [DllImport("advapi32.dll", SetLastError = true)]
        private static extern bool SetServiceStatus(IntPtr handle, ref ServiceStatus serviceStatus);

        public ImportService()
        {
            InitializeComponent();

            //defining different loggers for thedifferent processes, so the log information can be routed to the proper DB table
            //taskLogger  = (DCLogger)LogManager.GetLogger("ImportDatabaseTaskLogger");
            //telogLogger = (DCLogger)LogManager.GetLogger("ImportDatabaseTellogLogger");

            taskLogger.NLog = LogManager.GetLogger("ImportDatabaseTaskLogger");
            //telogLogger = (DCLogger)LogManager.GetLogger("ImporterFileTask");
            logger.NLog = LogManager.GetLogger("ImporterFileTask");


            // ILGenerator.
            //logger.Trace("Start Import Service");
            DataImportServiceEheventLog = new System.Diagnostics.EventLog();
            if (!System.Diagnostics.EventLog.SourceExists("DataImportService"))
            {
                System.Diagnostics.EventLog.CreateEventSource(
                    "DataImportService", "DataImportServiceLog");
            }
            DataImportServiceEheventLog.Source = "DataImport";
            DataImportServiceEheventLog.Log = "";
        }

        protected override void OnStart(string[] args)
        {
            int taskinterval = 30000;
            int fileinterval = 90000;
            int dbinterval = 300000;

            if (!(string.IsNullOrEmpty(ConfigurationManager.AppSettings["TaskScaningInterval"])))
            {
                if (!int.TryParse(ConfigurationManager.AppSettings["TaskScaningInterval"], out taskinterval))
                {
                    taskinterval = 30000;
                }
            }

            if (!(string.IsNullOrEmpty(ConfigurationManager.AppSettings["FileScaningInterval"])))
            {
                if (!int.TryParse(ConfigurationManager.AppSettings["FileScaningInterval"], out fileinterval))
                {
                    fileinterval = 90000;
                }
            }

            if (!(string.IsNullOrEmpty(ConfigurationManager.AppSettings["DataBaseScaningInterval"])))
            {
                if (!int.TryParse(ConfigurationManager.AppSettings["DataBaseScaningInterval"], out dbinterval))
                {
                    dbinterval = 300000;
                }
            }

            // Update the service state to Start Pending.
            ServiceStatus serviceStatus = new ServiceStatus();
            serviceStatus.dwCurrentState = BaseEnum.ServiceState.SERVICE_START_PENDING;
            serviceStatus.dwWaitHint = 100000;
            SetServiceStatus(this.ServiceHandle, ref serviceStatus);

            // Set up a timer to trigger to scan task tables.
            System.Timers.Timer timerTaskScan = new System.Timers.Timer();
            timerTaskScan.Interval = taskinterval; // 15 minutes
            timerTaskScan.Elapsed += new System.Timers.ElapsedEventHandler(this.OnTaskTimer);
            timerTaskScan.Start();

            // Set up a timer to trigger to scan file system for new files.
            System.Timers.Timer timerFileScan = new System.Timers.Timer();
            timerFileScan.Interval = fileinterval;
            timerFileScan.Elapsed += new System.Timers.ElapsedEventHandler(this.OnFileScanTimer);
            timerFileScan.Start();


            // Set up a timer to trigger to scan databases for new imports.
            System.Timers.Timer timerDataBaseScan = new System.Timers.Timer();
            timerDataBaseScan.Interval = dbinterval;
            timerDataBaseScan.Elapsed += new System.Timers.ElapsedEventHandler(this.OnDBScanTimer);
            timerDataBaseScan.Start();

            // Update the service state to Running.
            serviceStatus.dwCurrentState = BaseEnum.ServiceState.SERVICE_RUNNING;
            SetServiceStatus(this.ServiceHandle, ref serviceStatus);
        }


        public void OnTaskTimer(object sender, System.Timers.ElapsedEventArgs args)
        {
            UnityContainer _container = new UnityContainer();
            _container.RegisterType<IMonitoringImportTask, MonitoringImportTask>();

            string undopath = String.Empty;
            if (!(string.IsNullOrEmpty(ConfigurationManager.AppSettings["UndoTaskPath"])))
            {
                undopath = ConfigurationManager.AppSettings["DataBaseScaningInterval"];
            }

            if (!(string.IsNullOrEmpty(ConfigurationManager.AppSettings["MonitoringImportTask"])))
            {

                var connection = ConfigurationManager.AppSettings["MonitoringImportTask"];
                if ((MonitoringImportTask != null) && (MonitoringImportTask.IsCompleted == false ||
                           MonitoringImportTask.Status == TaskStatus.Running ||
                           MonitoringImportTask.Status == TaskStatus.WaitingToRun ||
                           MonitoringImportTask.Status == TaskStatus.WaitingForActivation))
                {
                    logger.NLog.Trace("MonitoringImportTask Task has attempted to start while already running.  Will try on next cycle");
                }
                else
                {
                    logger.NLog.Trace("MonitoringImportTask Task has began");
                    MonitoringImportTask = Task.Factory.StartNew(() =>
                    {
                        var instance = _container.Resolve<IMonitoringImportTask>();
                        instance.StartMonitoringImportTask(connection, undopath, ref taskLogger);
                    });
                }
            }

        }


        public void OnDBScanTimer(object sender, System.Timers.ElapsedEventArgs args)
        {
            UnityContainer _container = new UnityContainer();
            _container.RegisterType<ITelogDB, TelogDB>();
            _container.RegisterType<IDetectronicDirectory, DetectronicDirectory>();

            if (!(string.IsNullOrEmpty(ConfigurationManager.AppSettings["TelogDB"])))
            {

                //  var connection = ConfigurationManager.AppSettings["TelogDB"];
                if ((TelogDB != null) && (TelogDB.IsCompleted == false ||
                           TelogDB.Status == TaskStatus.Running ||
                           TelogDB.Status == TaskStatus.WaitingToRun ||
                           TelogDB.Status == TaskStatus.WaitingForActivation))
                {
                    logger.NLog.Trace("TelogDB Task has attempted to start while already running.  Will try on next cycle");
                }
                else
                {
                    logger.NLog.Trace("TelogDB Task has began");
                    TelogDB = Task.Factory.StartNew(() =>
                    {
                        var instance = _container.Resolve<ITelogDB>();
                        instance.StartTelogDB(ref telogLogger);
                    });
                }
            }

            if (!(string.IsNullOrEmpty(ConfigurationManager.AppSettings["DetectronicDirectory"])))
            {


                var atytribValue = ConfigurationManager.AppSettings["DetectronicDirectory"];



                if (atytribValue != null)
                {
                    //if (atytribValue > 0)
                    //{
                    if ((DetectronicDirectory != null) && (DetectronicDirectory.IsCompleted == false ||
                               DetectronicDirectory.Status == TaskStatus.Running ||
                               DetectronicDirectory.Status == TaskStatus.WaitingToRun ||
                               DetectronicDirectory.Status == TaskStatus.WaitingForActivation))
                    {
                        logger.NLog.Trace("DetectronicDirectory Task has attempted to start while already running.  Will try on next cycle");
                    }
                    else
                    {
                        logger.NLog.Trace("DetectronicDirectory Task has began");
                        DetectronicDirectory = Task.Factory.StartNew(() =>
                        {
                            var instance = _container.Resolve<IDetectronicDirectory>();
                            instance.StartDetectronicDirectory(ref logger);
                        });
                    }
                    // }
                }
            }

        }



        public void OnFileScanTimer(object sender, System.Timers.ElapsedEventArgs args)
        {

            UnityContainer _container = new UnityContainer();
            _container.RegisterType<IIsodaqXMLPath, IsodaqXMLPath>();
            _container.RegisterType<IADSFTP, ADSFTP>();
            _container.RegisterType<IADSProfile, ADSProfile>();
            _container.RegisterType<IFLWKMap, FLWKMap>();
            _container.RegisterType<IXRSPath, XRSPath>();
            _container.RegisterType<IHOBOUX120, HOBOUX120>();
            _container.RegisterType<IRemoteSense, RemoteSense>();


            if (!(string.IsNullOrEmpty(ConfigurationManager.AppSettings["ADSFTP"])))
            {

                var path = ConfigurationManager.AppSettings["ADSFTP"];
                if ((ADSFTP != null) && (ADSFTP.IsCompleted == false ||
                           ADSFTP.Status == TaskStatus.Running ||
                           ADSFTP.Status == TaskStatus.WaitingToRun ||
                           ADSFTP.Status == TaskStatus.WaitingForActivation))
                {
                    logger.NLog.Trace("ADSFTP Task has attempted to start while already running.  Will try on next cycle");
                }
                else
                {
                    logger.NLog.Trace("ADSFTP Task has began");
                    ADSFTP = Task.Factory.StartNew(() =>
                    {
                        var instance = _container.Resolve<IADSFTP>();
                        instance.StartADS(path, ref logger);
                    });
                }
            }

            if (!(string.IsNullOrEmpty(ConfigurationManager.AppSettings["HOBOUX120"])))
            {

                var path = ConfigurationManager.AppSettings["HOBOUX120"];
                if ((HOBOUX120Task != null) && (HOBOUX120Task.IsCompleted == false ||
                           HOBOUX120Task.Status == TaskStatus.Running ||
                           HOBOUX120Task.Status == TaskStatus.WaitingToRun ||
                           HOBOUX120Task.Status == TaskStatus.WaitingForActivation))
                {
                    logger.NLog.Trace("HOBOUX120 Task has attempted to start while already running.  Will try on next cycle");
                }
                else
                {
                    logger.NLog.Trace("HOBOware Task has began");
                    HOBOUX120Task = Task.Factory.StartNew(() =>
                    {
                        var instance = _container.Resolve<IHOBOUX120>();
                        instance.StartHOBOUX120(path, ref logger);
                    });
                }
            }

            if (!(string.IsNullOrEmpty(ConfigurationManager.AppSettings["RemoteSense"])))
            {

                var path = ConfigurationManager.AppSettings["RemoteSense"];
                if ((RemoteSense != null) && (RemoteSense.IsCompleted == false ||
                           RemoteSense.Status == TaskStatus.Running ||
                           RemoteSense.Status == TaskStatus.WaitingToRun ||
                           RemoteSense.Status == TaskStatus.WaitingForActivation))
                {
                    logger.NLog.Trace("RemoteSense Task has attempted to start while already running.  Will try on next cycle");
                }
                else
                {
                    logger.NLog.Trace("RemoteSense Task has began");
                    RemoteSense = Task.Factory.StartNew(() =>
                    {
                        var instance = _container.Resolve<IRemoteSense>();
                        instance.StartRemoteSense(path, ref logger);
                    });
                }
            }


     //       RemoteSense RemoteSense

            if (!(string.IsNullOrEmpty(ConfigurationManager.AppSettings["IADSProfile"])))
            {

                var path = ConfigurationManager.AppSettings["IADSProfile"];
                if ((ADSProfile != null) && (ADSProfile.IsCompleted == false ||
                           ADSProfile.Status == TaskStatus.Running ||
                           ADSProfile.Status == TaskStatus.WaitingToRun ||
                           ADSProfile.Status == TaskStatus.WaitingForActivation))
                {
                    logger.NLog.Trace("IADSProfile Task has attempted to start while already running.  Will try on next cycle");
                }
                else
                {
                    logger.NLog.Trace("ADSProfile Task has began");
                    ADSProfile = Task.Factory.StartNew(() =>
                    {
                        var instance = _container.Resolve<IADSProfile>();
                        instance.StartADSProfile(path, ref logger);
                    });

                }
            }

            if (!(string.IsNullOrEmpty(ConfigurationManager.AppSettings["IsodaqXMLPath"])))
            {

                var path = ConfigurationManager.AppSettings["IsodaqXMLPath"];
                if ((IsodaqXMLPath != null) && (IsodaqXMLPath.IsCompleted == false ||
                           IsodaqXMLPath.Status == TaskStatus.Running ||
                           IsodaqXMLPath.Status == TaskStatus.WaitingToRun ||
                           IsodaqXMLPath.Status == TaskStatus.WaitingForActivation))
                {
                    logger.NLog.Trace("IsodaqXMLPath Task has attempted to start while already running.  Will try on next cycle");
                }
                else
                {
                    logger.NLog.Trace("IsodaqXMLPath Task has began");
                    IsodaqXMLPath = Task.Factory.StartNew(() =>
                    {
                        var instance = _container.Resolve<IsodaqXMLPath>();
                        instance.StartIsodaqXMLPath(path, ref logger);
                    });
                }
            }



            if (!(string.IsNullOrEmpty(ConfigurationManager.AppSettings["XRSPath"])))
            {

                var path = ConfigurationManager.AppSettings["XRSPath"];
                if ((XRSPath != null) && (XRSPath.IsCompleted == false ||
                           XRSPath.Status == TaskStatus.Running ||
                           XRSPath.Status == TaskStatus.WaitingToRun ||
                           XRSPath.Status == TaskStatus.WaitingForActivation))
                {
                    logger.NLog.Trace("XRSPath Task has attempted to start while already running.  Will try on next cycle");
                }
                else
                {
                    logger.NLog.Trace("XRSPath Task has began");
                    XRSPath = Task.Factory.StartNew(() =>
                    {
                        var instance = _container.Resolve<IXRSPath>();
                        instance.StartXRSPath(path, ref logger);
                    });
                }
            }


            if (!(string.IsNullOrEmpty(ConfigurationManager.AppSettings["FLWKMap"])))
            {

                var path = ConfigurationManager.AppSettings["FLWKMap"];
                if ((FLWKMap != null) && (FLWKMap.IsCompleted == false ||
                           FLWKMap.Status == TaskStatus.Running ||
                           FLWKMap.Status == TaskStatus.WaitingToRun ||
                           FLWKMap.Status == TaskStatus.WaitingForActivation))
                {
                    logger.NLog.Trace("FLWKMap Task has attempted to start while already running.  Will try on next cycle");
                }
                else
                {
                    logger.NLog.Trace("XRSPath Task has began");
                    FLWKMap = Task.Factory.StartNew(() =>
                    {
                        var instance = _container.Resolve<IFLWKMap>();
                        instance.StartFLWKMap(path, ref logger);
                    });
                }
            }

        }


        protected override void OnStop()
        {
            // TODO: Add code here to perform any tear-down necessary to stop your service.
        }
    }
}
