﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Practices.Unity;
using Civica.BLL.ServiceClasses.DataImporter.Interfaces;
using Civica.BLL.ServiceClasses.DataImporter;

namespace DataImporter
{
    static class Program
    {

       
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main()
        {
            ServiceBase[] ServicesToRun;
            ServicesToRun = new ServiceBase[]
            {
                new ImportService()
            };
            ServiceBase.Run(ServicesToRun);
        }
    }
}
