﻿using System;
using System.Configuration;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading;
using System.Threading.Tasks;


namespace DataImporter
{
    public partial class DataImporterService : ServiceBase
    {
        public DataImporterService()
        {
            InitializeComponent();
            eventLog1 = new System.Diagnostics.EventLog();
            if (!System.Diagnostics.EventLog.SourceExists("DataImporterSerivice"))
            {
                System.Diagnostics.EventLog.CreateEventSource(
                    "DataImporterSerivice", "DataImporterSeriviceLog");
            }
            eventLog1.Source = "DataImporterSerivice";
            eventLog1.Log = "DataImporterSeriviceLog";
        }

        protected override void OnStart(string[] args)
        {
            eventLog1.WriteEntry("Data Importer Service is Starting");

            Thread startThread = new Thread(Init);
            startThread.IsBackground = false;
            startThread.Start();
        }

        protected override void OnStop()
        {
            eventLog1.WriteEntry("Stopping Data Importer Service");
        }

        protected override void OnContinue()
        {
            eventLog1.WriteEntry("DataImporter Service is Continung Execution.");
        }

        protected void Init()
        {
            Thread.Sleep(1000 * 60 * 3);
            var appSettings = ConfigurationManager.AppSettings;

            if (appSettings.Count == 0)
            {
                eventLog1.WriteEntry("AppSettings are empty");
            }
        }
    }
}
