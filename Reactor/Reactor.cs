﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Configuration;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;
using System.IO;
using Microsoft.Practices.Unity;
using Civica.BLL.ServiceClasses.Reactor.Interfaces;
using Civica.BLL.ServiceClasses.Reactor;

namespace Reactor
{
    public partial class Reactor : ServiceBase
    {
        private Task IPProcess;
        private Task BEProcess;
        private List<LTask> runningtasks = new List<LTask>();

        public List<int> IpPlist = new List<int>();
        public List<int> BEPlist = new List<int>();
        // System.Diagnostics.EventLog eLog = new System.Diagnostics.EventLog();
        public Reactor()
        {
            InitializeComponent();
            if (!System.Diagnostics.EventLog.SourceExists("Reactor"))
            {
                System.Diagnostics.EventLog.CreateEventSource(
                    "Reactor", "Reactor");
            }
            //eLog.Source = "Reactor";
        }

        protected override void OnStart(string[] args)
        { 
            string localEndpoing = String.Empty;
            string portsList = String.Empty;


            if (!(string.IsNullOrEmpty(ConfigurationManager.AppSettings["ListeningLocalEndPoint"])))
            {
                localEndpoing = ConfigurationManager.AppSettings["ListeningLocalEndPoint"];

                EventLog.WriteEntry("Local End Point is :" + localEndpoing);

                if (!(string.IsNullOrEmpty(ConfigurationManager.AppSettings["ListeningPortsIPRegistretion"])))
                {
                    portsList = ConfigurationManager.AppSettings["ListeningPortsIPRegistretion"];
                    var ports = portsList.Split(',');
                    int port = 0;
                    if (int.TryParse(portsList, out port))
                    {
                        EventLog.WriteEntry("Starting StartIPRegTask");
                        StartIPRegTask(port, localEndpoing);
                    }
                }


                if (!(string.IsNullOrEmpty(ConfigurationManager.AppSettings["ListeningPortsBEvent"])))
                {
                    portsList = ConfigurationManager.AppSettings["ListeningPortsBEvent"];
                    var ports = portsList.Split(',');
                    int port = 0;
                    if (int.TryParse(portsList, out port))
                    {
                        EventLog.WriteEntry("Starting StartBEventTask");
                        StartBEventTask(port, localEndpoing);
                    }
                }
            }

            EventLog.WriteEntry("End of On start");
        }




        protected override void OnStop()
        {
            EventLog.WriteEntry("Finesh with the servic");
        }


        public void StartIPRegTask(int port, string endpoit)
        {
            UnityContainer _container = new UnityContainer();
            _container.RegisterType<IReactorServices, ReactorServices>();
            IPProcess = Task.Factory.StartNew(() =>
                {
                    EventLog.WriteEntry("Starting StartIPRegTask factory");
                    var instance = _container.Resolve<IReactorServices>();
                    instance.StartIPReg(port, endpoit, EventLog);
                });

        }

        public void StartBEventTask(int port, string endpoit)
        {
            UnityContainer _container = new UnityContainer();
            _container.RegisterType<IReactorServices, ReactorServices>();

            BEProcess = Task.Factory.StartNew(() =>
                {
                    EventLog.WriteEntry("Starting StartBEventTask factory");
                    var instance = _container.Resolve<IReactorServices>();
                    instance.StartBEvent(port, endpoit, EventLog);
                });

        }

    }


    public class LTask
    {
        public Task Task;
        public int Port;
    }
}

