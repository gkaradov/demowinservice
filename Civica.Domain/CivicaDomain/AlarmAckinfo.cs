//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Civica.Domain.CivicaDomain
{
    using System;
    using System.Collections.Generic;
    
    public partial class AlarmAckinfo
    {
        public string GuidId { get; set; }
        public int AlarmId { get; set; }
        public string EmailAddress { get; set; }
        public System.DateTimeOffset EmailSendTime { get; set; }
        public Nullable<System.DateTimeOffset> AckTime { get; set; }
        public int Id { get; set; }
    
        public virtual MonitoringStationAlarm MonitoringStationAlarm { get; set; }
    }
}
