//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Civica.Domain.CivicaDomain
{
    using System;
    using System.Collections.Generic;
    
    public partial class ProjectLayerGroup
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public ProjectLayerGroup()
        {
            this.ProjectGeometryLayers = new HashSet<ProjectGeometryLayer>();
            this.ProjectLayerGroup1 = new HashSet<ProjectLayerGroup>();
        }
    
        public int Id { get; set; }
        public int ProjectId { get; set; }
        public Nullable<int> Parentid { get; set; }
        public string Name { get; set; }
        public bool Active { get; set; }
        public bool isEssential { get; set; }
        public Nullable<int> SortOrder { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ProjectGeometryLayer> ProjectGeometryLayers { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ProjectLayerGroup> ProjectLayerGroup1 { get; set; }
        public virtual ProjectLayerGroup ProjectLayerGroup2 { get; set; }
        public virtual Project Project { get; set; }
    }
}
