//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Civica.Domain.CivicaDomain
{
    using System;
    using System.Collections.Generic;
    
    public partial class MonitoringSensorEquation
    {
        public int Id { get; set; }
        public int SensorId { get; set; }
        public Nullable<System.DateTimeOffset> ValidUntil { get; set; }
        public string Equation { get; set; }
        public string Comments { get; set; }
        public Nullable<int> MonitoringLoggerChannelId { get; set; }
        public bool Active { get; set; }
    
        public virtual MonitoringLoggerChannel MonitoringLoggerChannel { get; set; }
        public virtual MonitoringSensor MonitoringSensor { get; set; }
    }
}
