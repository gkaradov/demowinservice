﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Civica.Domain.CivicaDomain;
using System.Text.RegularExpressions;
using Civica.Infrastructure.BaseClasses;

namespace Civica.BLL.Alarm
{
    public abstract class CondChecker
    {
        protected int condIdx { get; set; }      // the idx of the condition of the alarm
        public string result { get; set; }
        public DateTimeOffset? TestTime { get; set; }
        public DateTimeOffset? LastRun { get; set; }
        public DateTimeOffset? CheckToTime { get; set; }
        public CondChecker(int condIdx)
        {

            this.condIdx = condIdx;
        }

        /**
   * Return the alarm parameter type at the specified index
   * @return string
   */
        public List<String> getParamTypes()
        {
            var model = new datacurrentEntities();
            var CondtionTypeParameterTypes = model.MonitoringStationAlarmConditions.Where(x => x.Id == this.condIdx).FirstOrDefault().MonitoringStationAlarmConditionType.ParameterTypes;
            var parameterTypes = new List<String>();
            if (CondtionTypeParameterTypes != null)
            {
                parameterTypes = CondtionTypeParameterTypes.Split(';').ToList();
            }
            return parameterTypes;

        }
        public List<String> getParameters()
        {
            var model = new datacurrentEntities();
            var parameter = model.MonitoringStationAlarmConditions.Where(x => x.Id == this.condIdx).Select(x => x.Params).FirstOrDefault();
            var parameterTypes = new List<String>();
            if (parameter != null)
            {
                parameterTypes = parameter.Split(';').ToList();
            }
            return parameterTypes;

        }



        /**
    * Make sure that the alarm parameters are of the correct type and return
    * an array of error messages.
    * @return array
    */
        public string GetErrors(System.Diagnostics.EventLog log)
        {
            var model = new datacurrentEntities();
            string errors = "";
            // make sure all the parameter types are ok
            var parametypes = getParamTypes();
            var paramters = getParameters();
            var conditionTypeId = model.MonitoringStationAlarmConditions.Where(x => x.Id == this.condIdx).FirstOrDefault().AlarmConditionTypeId;
            if (parametypes.Count != paramters.Count)
            {
                errors = "Expected " + parametypes.Count + " parameters, got " + paramters.Count;
                return errors;
            }
            for (var i = 0; i < paramters.Count; i++)
            {
                switch (parametypes[i])
                {
                    case "s":
                        if (conditionTypeId == 10)
                        {
                            if (i == 0)
                            {
                                var sensorIds = model.MonitoringSensorAlarms.Where(x => x.MonitoringStationAlarmConditionId == this.condIdx).Where(x => x.TypeA == true).Where(x => x.Active == true).Select(x => x.MonitoringSensorId);
                                var param = paramters[i];
                                var sensors = param.Replace("{", "").Replace("}", "").Split(',');
                                if (sensorIds.Count() != sensors.Count())
                                {
                                    errors = "Invalid sensor group " + param;
                                }
                                else
                                {
                                    foreach (var sensorId in sensors)
                                    {
                                        if (!sensors.Contains(Convert.ToString(sensorId)))
                                        {
                                            errors = "Invalid sensor: " + sensorId;
                                        }
                                    }
                                }
                            }
                            else
                            {
                                var sensorIds = model.MonitoringSensorAlarms.Where(x => x.MonitoringStationAlarmConditionId == this.condIdx).Where(x => x.TypeB == true).Where(x => x.Active == true).Select(x => x.MonitoringSensorId);
                                var param = paramters[i];
                                var sensors = param.Replace("{", "").Replace("}", "").Split(',');
                                if (sensorIds.Count() != sensors.Count())
                                {
                                    errors = "Invalid sensor group " + param;
                                }
                                else
                                {
                                    foreach (var sensorId in sensors)
                                    {
                                        if (!sensors.Contains(Convert.ToString(sensorId)))
                                        {
                                            errors = "Invalid sensor: " + sensorId;
                                        }
                                    }
                                }
                            }

                        }
                        else
                        {
                            var sensorIds = model.MonitoringSensorAlarms.Where(x => x.MonitoringStationAlarmConditionId == this.condIdx).Where(x => x.Active == true).Select(x => x.MonitoringSensorId);
                            var param = paramters[i];
                            var sensors = param.Replace("{", "").Replace("}", "").Split(',');
                            if (sensorIds.Count() != sensors.Count())
                            {
                                errors = "Invalid sensor group " + param;
                            }
                            else
                            {
                                foreach (var sensorId in sensors)
                                {
                                    if (!sensors.Contains(Convert.ToString(sensorId)))
                                    {
                                        errors = "Invalid sensor: " + sensorId;
                                    }
                                }
                            }
                        }

                        break;
                    case "d":
                        // double
                        double outnumber;
                        bool isnumber = double.TryParse(paramters[i], out outnumber);
                        if (!isnumber)
                        {
                            errors = errors + paramters[i] + "' must be numeric";

                        }
                        break;
                    case "ud":
                        // unsigned double

                        isnumber = double.TryParse(paramters[i], out outnumber);
                        if (!isnumber)
                        {
                            errors = errors + paramters[i] + "' must be numeric";
                        }
                        else
                        {
                            if (outnumber < 0)
                            {
                                errors = errors + paramters[i] + "' must be positive";
                            }
                        }
                        break;
                    case "i":
                        int intOut;
                        isnumber = int.TryParse(paramters[i], out intOut);
                        if (!isnumber)
                        {
                            errors = errors + paramters[i] + "' must be numeric";
                        }

                        break;
                    case "ui":

                        isnumber = int.TryParse(paramters[i], out intOut);
                        if (!isnumber)
                        {
                            errors = errors + paramters[i] + "' must be numeric";
                        }
                        else
                        {
                            if (intOut < 0)
                            {
                                errors = errors + paramters[i] + "' must be positive";
                            }
                        }
                        break;
                    case "e":
                        var equationValidate = ValidateEquation(paramters[i], log);
                        if (equationValidate != true)
                        {
                            errors = errors + paramters[i] + " is not a valid equation";
                        }
                        break;
                    default:
                        errors = errors + "Internal error: " + parametypes[i] + " is not a valid type";
                        break;

                }

            }

            return errors;
        }

        /**
     * Use the associated alarm condition checker to check for violations.
     * Return one of the ALARMRES constants which describe the result of the
     * check.
     * Note - If the $test_time param is specified, we consider the check to
     *        be only a "what-if" check.  This means that:
     *          1) the last_run time will not be updated in the database
     *          2) we don't care if the site is in maintenance mode or not
     *          3) we don't care if sensors are offline or not
     * @param optional $test_time string Alarm will be checked as if this was the current time (yyyy-mm-dd hh:mm:ss)
     * @param optional $test_lastrun string Override the last_run value stored in the database.  This is the number of minutes
     *                                      to be subtracted from $test_time.
     * @throws Exception if $testtime is invalid
     * @return ALARMRES
     */
        public BaseEnum.AlarmTriggerType Check(DateTimeOffset? testTime, DateTimeOffset? testLastRun, System.Diagnostics.EventLog log)
        {
            var model = new datacurrentEntities();
            var errors = GetErrors(log);

            var stationId = model.MonitoringStationAlarmConditions.Where(x => x.Id == this.condIdx).FirstOrDefault().MonitoringStationAlarm.MonitoringStationId;
            var stationTimeOffSet = model.MonitoringStations.Where(x => x.Id == stationId).FirstOrDefault().TimeZone.Offset;
            var stationTimeZoneSpan = new TimeSpan(Convert.ToInt32(stationTimeOffSet), 0, 0);
            var station = model.MonitoringStations.Where(x => x.Id == stationId).FirstOrDefault();
            var alarm = model.MonitoringStationAlarmConditions.Where(x => x.Id == this.condIdx).FirstOrDefault().MonitoringStationAlarm;
            if (errors != null && errors != "")
            {
                throw new Exception(errors);
            }
            var sensors = model.MonitoringSensorAlarms.Where(x => x.MonitoringStationAlarmConditionId == this.condIdx).Where(x => x.Active == true).Select(x => x.MonitoringSensor);
            var condtionTypeId = model.MonitoringStationAlarmConditions.Where(x => x.Id == condIdx).FirstOrDefault().AlarmConditionTypeId;
            TestTime = null;
            CheckToTime = null;
            if (testLastRun == null)
            {
                if (alarm.LastRun != null)
                    testLastRun = alarm.LastRun.Value;
                else
                {
                    testLastRun = DateTimeOffset.MaxValue.ToOffset(stationTimeZoneSpan);
                    foreach (var sensor in sensors)
                    {
                        var TimeStamps = model.MonitoringDatas.Where(x => x.MonitoringSensorId == sensor.Id).OrderBy(x => x.TimeStamp).Select(x => x.TimeStamp);
                        if (TimeStamps.Count() != 0)
                        {
                            var firstTimeStamp = TimeStamps.FirstOrDefault();
                            if (condtionTypeId != 4 && condtionTypeId != 9)
                            {
                                if (firstTimeStamp < testLastRun)
                                {
                                    testLastRun = firstTimeStamp;
                                }
                            }
                        }


                    }

                }

            }
            if (testLastRun != null)
                testLastRun = testLastRun.Value.ToOffset(stationTimeZoneSpan);
            else
                testLastRun = DateTimeOffset.Now.ToOffset(stationTimeZoneSpan);
            bool conidtionSensorHasData = false;
            if (testTime == null)
            {
                testTime = DateTimeOffset.Now.ToOffset(stationTimeZoneSpan);
                if (station.MaintanaceMode)
                {
                    return BaseEnum.AlarmTriggerType.ALARMRES_OK;
                }
                foreach (var sensor in sensors)
                {
                    if (sensor.SensorStatusId == 5)
                    {
                        return BaseEnum.AlarmTriggerType.ALARMRES_NODATA;
                    }
                    var TimeStamps = model.MonitoringDatas.Where(x => x.MonitoringSensorId == sensor.Id).Where(x => x.TimeStamp > testLastRun).OrderByDescending(x => x.TimeStamp).Select(x => x.TimeStamp);
                    if (TimeStamps.Count() != 0)
                    {
                        conidtionSensorHasData = true;
                        var lastTimeStamp = TimeStamps.FirstOrDefault();

                        if (condtionTypeId != 4 && condtionTypeId != 9)
                        {
                            if (lastTimeStamp < testTime)
                            {
                                testTime = lastTimeStamp;

                            }
                        }
                        if (alarm.UserPointSensorCalculateLastRun)
                        {
                            if (condtionTypeId != 4 && condtionTypeId != 9)
                            {
                                if (TestTime == null)
                                {
                                    TestTime = testTime;
                                }
                                else
                                {
                                    if (TestTime.Value > testTime)
                                    {
                                        TestTime = testTime;
                                    }
                                }
                            }
                        }
                        else
                        {
                            if (condtionTypeId != 4 && condtionTypeId != 9 && sensor.SensorDataTypeId != 1)
                            {
                                if (TestTime == null)
                                {
                                    TestTime = testTime;
                                }
                                else
                                {
                                    if (TestTime.Value > testTime)
                                    {
                                        TestTime = testTime;
                                    }
                                }
                            }
                        }


                    }


                }


            }
            if (condtionTypeId != 4 && condtionTypeId != 9)
            {
                if (conidtionSensorHasData == false)
                {
                    return BaseEnum.AlarmTriggerType.ALARMRES_NODATA;
                }
            }
            if (TestTime != null)
            {
                CheckToTime = TestTime;
            }
            foreach (var sensor in sensors)
            {
                if (sensor.SensorDataTypeId == 2)
                {
                    //if (sensor.TimeInterval != null)
                    //{
                        var factor = Convert.ToDouble(60 * sensor.TimeInterval);
                        var testDateTimeSeconds = Convert.ToInt64(testTime.Value.DateTime.ToOADate() * 3600 * 24);

                        var testTime2 = (double)(Math.Ceiling(testDateTimeSeconds / factor) * factor);
                        if (testTime2 > testDateTimeSeconds)
                        {
                            double ds1 = testTime2;
                            var dateTime = DateTime.FromOADate(ds1 / (3600 * 24));
                            testTime = new DateTimeOffset(dateTime, stationTimeZoneSpan);
                        }

                    //}

                }
            }

            if (alarm.LastRun != null)
                this.LastRun = alarm.LastRun.Value.ToOffset(stationTimeZoneSpan);
            else
                this.LastRun = DateTimeOffset.Now.ToOffset(stationTimeZoneSpan);

            return this._Check(testTime.Value, testLastRun.Value);





        }

        /**
        * The type of alarm condition that this class will check
        * @return string
*/
        abstract public string CondType();

        /**
        * Use the associated alarm condition checker to check for violations.
        * Return one of the ALARMRES constants which describe the result of the
        * check
        * @param $time int Unix timestamp which the alarm checker will use as the current time
        * @return ALARMRES
*/
        abstract public BaseEnum.AlarmTriggerType _Check(DateTimeOffset testTime, DateTimeOffset TestLastrun);

        /**
         * Checker-type-specific error checker
         * @return array An array of error messages (empty if no errors)
         */
        abstract protected string _GetErrors();



        /**
    * Make sure that the alarm parameters are of the correct type and return
    * an array of error messages.
    * @return array
    */
        public bool ValidateEquation(string equation, System.Diagnostics.EventLog log)
        {
            double? result = 0;
            try
            {
                equation = Regex.Replace(equation, @"\{[Bb]\}", "1000");
                result = (new Calculation()).evaluate(equation);
                if (result == null)
                {
                    log.WriteEntry("ValidateEquation result is null");
                }
                else
                {
                    if (double.IsNaN(result.Value) && double.IsInfinity(result.Value))
                    {
                        log.WriteEntry("ValidateEquation result value cannot be infinit");
                    }
                }

            }
            catch (Exception ex)
            {
                log.WriteEntry("Exception ; " + ex.Message);
            }
            return true;
        }

        protected double? Evaluate(string equation, string value)
        {
            double? result = 0;
            try
            {
                equation = Regex.Replace(equation, @"\{[Bb]\}", value);
                result = (new Calculation()).evaluate(equation);
                if (result == null)
                {
                    throw new Exception("result is null");
                }
                else
                {
                    if (double.IsNaN(result.Value) && double.IsInfinity(result.Value))
                    {
                        throw new Exception("result value cannot be infinit");
                    }
                }

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return result;
        }

    }
}
