﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Civica.Domain.CivicaDomain;
using System.Data.Entity;
using Civica.Infrastructure.BaseClasses;

namespace Civica.BLL.Alarm
{

    public class TimeSumMax : CondChecker
    {
        public TimeSumMax(int condIdx) :
         base(condIdx)
        {
        }

        public override string CondType()
        {
            return "TIMESUM_MAX";
        }
        /**
     * Trigger if the sum of all values over the specified number of hours is 
     * greater than the specified maximum
     * @return ALARMRES
     */
        public override BaseEnum.AlarmTriggerType _Check(DateTimeOffset testTime, DateTimeOffset testLastRun)
        {
            var model = new datacurrentEntities();

            var monitoringSensorIds = model.MonitoringSensorAlarms.Where(x => x.MonitoringStationAlarmConditionId == this.condIdx).Where(x => x.Active == true).Select(x => x.MonitoringSensorId);
            var monitoringSensors = model.MonitoringSensorAlarms.Where(x => x.MonitoringStationAlarmConditionId == this.condIdx).Where(x => x.Active == true).Select(x => x.MonitoringSensor).ToArray();
            var conditionparameter = model.MonitoringStationAlarmConditions.Where(x => x.Id == this.condIdx).Select(x => x.Params).FirstOrDefault();
            var sumHours = Convert.ToDouble(conditionparameter.Split(';')[2]);
            var sumValue = Convert.ToDecimal(conditionparameter.Split(';')[1]);
            var newTestTime = testTime;
         
            List<decimal> sumValueList = new List<decimal>();
            var newStartTime = testTime;
            while (DateTimeOffset.Compare(newStartTime, testLastRun) > 0)
            {
                newStartTime = newStartTime.AddHours((-1) * sumHours);
            }
            var data = model.MonitoringDatas.Where(x => monitoringSensorIds.Contains(x.MonitoringSensorId)).Where(x => x.TimeStamp <= testTime).Where(x => x.TimeStamp > newStartTime);
          
            if (data.Count() == 0)
            {
                bool points = true;
                foreach (var monitoringsensor in monitoringSensors)
                {
                    if (monitoringsensor.SensorDataTypeId != 1)
                    {
                        points = false;
                    }
                }
                if (points)
                {
                    if (sumValue < 0)
                    {
                        var sensor = monitoringSensors[0];
                        // build the result
                        this.result = "Sensors:";
                        for (var j = 0; j < monitoringSensors.Count(); j++)
                        {
                            if (j != monitoringSensors.Count() - 1)
                            {
                                this.result = this.result + monitoringSensors[j].Name + "(" + monitoringSensors[j].Id + ")" + ", ";
                            }
                            else
                            {
                                this.result = this.result + monitoringSensors[j].Name + "(" + monitoringSensors[j].Id + ")";
                            }
                        }
                        this.result = this.result + "\n";
                        this.result = this.result + "No New Data " + "From " + testLastRun.ToString() + " To " + testTime.ToString();
                        return BaseEnum.AlarmTriggerType.ALARMRES_VIOLATION;

                    }
                    else
                        return BaseEnum.AlarmTriggerType.ALARMRES_OK;
                }
                else
                    return BaseEnum.AlarmTriggerType.ALARMRES_NODATA;
            }
            if (sumValue < 0)
            {
                foreach (var monitoringsensor in monitoringSensors)
                {
                    if (monitoringsensor.SensorDataTypeId == 1)
                    {
                        var PointSensorData = data.Where(x => x.MonitoringSensorId == monitoringsensor.Id);
                        if (PointSensorData.Count() == 0)
                        {

                            this.result = this.result + monitoringsensor.Name + "(" + monitoringsensor.Id + ") " + "No New Data From" + testLastRun.ToString() + " To " + testTime.ToString();
                            return BaseEnum.AlarmTriggerType.ALARMRES_VIOLATION;
                        }
                    }
                }
            }
            var checkFromTimes = new List<DateTimeOffset>();
            var checkToTimes = new List<DateTimeOffset>();
            var dictonarySumValue = new Dictionary<int, List<decimal>>();
            int index = 0;
            while (newTestTime > testLastRun)
            {
                testTime = newTestTime;
                newTestTime = newTestTime.AddHours((-1) * sumHours);
                sumValueList = data.Where(x => x.TimeStamp <= testTime).Where(x => x.TimeStamp > newTestTime).GroupBy(x => x.MonitoringSensorId).Select(x => new { sensorId = x.Key, sumValue = x.Sum(row => row.Value) }).Select(x => x.sumValue).ToList();
                checkFromTimes.Add(newTestTime);
                checkToTimes.Add(testTime);
                dictonarySumValue.Add(index, sumValueList);
                index++;
            }
            bool allValueOk = true;
            decimal VIOLATIONValue = 0;
            DateTimeOffset? ViolationFromTime = null;
            DateTimeOffset? ViolationToTime = null;

            foreach (var key in dictonarySumValue)
            {
                var sums = dictonarySumValue[key.Key];
                foreach (var sum in sums)
                {
                    if (sum >= sumValue)
                    {
                        allValueOk = false;
                        VIOLATIONValue = sum;
                        ViolationFromTime = checkFromTimes[key.Key];
                        ViolationToTime = checkToTimes[key.Key];
                        break;
                    }
                }
            }
            if (allValueOk)
                return BaseEnum.AlarmTriggerType.ALARMRES_OK;
            var Unitsensor = model.MonitoringSensors.Where(x => x.Id == monitoringSensorIds.FirstOrDefault()).FirstOrDefault();
            // build the result
            this.result = "Sensors:";
            for (var j = 0; j < monitoringSensors.Count(); j++)
            {
                if (j != monitoringSensors.Count() - 1)
                {
                    this.result = this.result + monitoringSensors[j].Name + "(" + monitoringSensors[j].Id + ")" + ", ";
                }
                else
                {
                    this.result = this.result + monitoringSensors[j].Name + "(" + monitoringSensors[j].Id + ")";
                }
            }
            this.result = this.result + "\n";
            this.result = this.result + "From " + ViolationFromTime.Value.ToString() + " To " + ViolationToTime.Value.ToString() + " Sum Value is " + VIOLATIONValue + " " + Unitsensor.Unit;
            return BaseEnum.AlarmTriggerType.ALARMRES_VIOLATION;
        }
        protected override string _GetErrors()
        {
            return "";
        }
    }
}
