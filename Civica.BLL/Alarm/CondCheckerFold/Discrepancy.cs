﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Civica.Domain.CivicaDomain;
using System.Data.Entity;
using Civica.Infrastructure.BaseClasses;

namespace Civica.BLL.Alarm
{
    public class Discrepancy : CondChecker
    {

        public Discrepancy(int condIdx) :
         base(condIdx)
        {
        }
        public override string CondType()
        {
            return "DISCREPANCY";
        }
        /**
     * For both Sensor A and Sensor B, get the average value of all the 
     * points in the last T minutes.  If the difference between the averages
     * is X or more, trigger an alarm.
     * @return ALARMRES
     */
        public override BaseEnum.AlarmTriggerType _Check(DateTimeOffset testTime, DateTimeOffset testLastRun)
        {
            var model = new datacurrentEntities();
            var conditionParam = model.MonitoringStationAlarmConditions.Where(x => x.Id == this.condIdx).Select(x => x.Params).FirstOrDefault();
            var sensorIdsA = model.MonitoringSensorAlarms.Where(x => x.MonitoringStationAlarmConditionId == this.condIdx).Where(x => x.Active == true).Where(x => x.TypeA == true).Select(x => x.MonitoringSensorId).ToList();
            var sensorIdsB = model.MonitoringSensorAlarms.Where(x => x.MonitoringStationAlarmConditionId == this.condIdx).Where(x => x.Active == true).Where(x => x.TypeB == true).Select(x => x.MonitoringSensorId).ToList();
            var equation = conditionParam.Split(';')[2];
            var delay = Convert.ToDouble(conditionParam.Split(';')[3]);
            var threshold = Convert.ToDecimal(conditionParam.Split(';')[4]);
            var minutes = Convert.ToDouble(conditionParam.Split(';')[5]);
            // Get the avg value of sensor A and B
            Dictionary<int, decimal> avgvalsA = new Dictionary<int, decimal>();

            decimal average = 0;
            foreach (var sensorId in sensorIdsA)
            {
                if (delay < 0)
                {
                    var startTime = testTime.AddMinutes(delay).AddMinutes((-1) * minutes);
                    var data = model.MonitoringDatas.Where(x => x.MonitoringSensorId == sensorId).Where(x => x.TimeStamp <= testTime.AddMinutes(delay)).Where(x => x.TimeStamp > startTime).Select(x => x.Value);
                    if (data.Count() > 0)
                    {
                        average = data.Average();
                        avgvalsA.Add(sensorId, average);
                    }

                }
                else
                {
                    var startTime = testTime.AddMinutes((-1) * minutes);
                    var data = model.MonitoringDatas.Where(x => x.MonitoringSensorId == sensorId).Where(x => x.TimeStamp <= testTime).Where(x => x.TimeStamp > startTime).Select(x => x.Value);
                    if (data.Count() > 0)
                    {
                        average = data.Average();
                        avgvalsA.Add(sensorId, average);
                    }

                }
            }
            if (avgvalsA.Count == 0)
                return BaseEnum.AlarmTriggerType.ALARMRES_NODATA;
            Dictionary<int, decimal> avgvalsB = new Dictionary<int, decimal>();

            average = 0;
            foreach (var sensorId in sensorIdsB)
            {
                if (delay < 0)
                {
                    var startTime = testTime.AddMinutes((-1) * minutes);
                    var data = model.MonitoringDatas.Where(x => x.MonitoringSensorId == sensorId).Where(x => x.TimeStamp <= testTime).Where(x => x.TimeStamp > startTime).Select(x => x.Value);
                    if (data.Count() > 0)
                    {
                        average = data.Average();
                        avgvalsB.Add(sensorId, average);
                    }
                }
                else
                {
                    var endTime = testTime.AddMinutes(delay);
                    var startTime = testTime.AddMinutes(delay).AddMinutes((-1) * minutes);
                    var data = model.MonitoringDatas.Where(x => x.MonitoringSensorId == sensorId).Where(x => x.TimeStamp <= endTime).Where(x => x.TimeStamp > startTime).Select(x => x.Value);
                    if (data.Count() > 0)
                    {
                        average = data.Average();
                        avgvalsB.Add(sensorId, average);
                    }
                }
            }
            if (avgvalsB.Count == 0)
                return BaseEnum.AlarmTriggerType.ALARMRES_NODATA;
            bool triggered = false;
            decimal valueA = 0;
            decimal valueB = 0;
            int sensorA = 0;
            int sensorB = 0;

            try
            {
                foreach (var val_a in avgvalsA)
                {
                    foreach (var val_b in avgvalsB)
                    {
                        if (Math.Abs(val_a.Value - Convert.ToDecimal(this.Evaluate(equation, Convert.ToString(val_b.Value)).Value)) >= threshold)
                        {
                            valueA = val_a.Value;
                            valueB = val_b.Value;
                            sensorA = val_a.Key;
                            sensorB = val_b.Key;

                            triggered = true;
                            break;

                        }
                    }
                }

            }
            catch (Exception ex)
            {
                return BaseEnum.AlarmTriggerType.ALARMRES_ERRORS;

            }
            if (!triggered)
                return BaseEnum.AlarmTriggerType.ALARMRES_OK;
            var sensor = model.MonitoringSensors.Where(x => x.Id == sensorIdsA.FirstOrDefault()).FirstOrDefault();
            var sensorAObject = model.MonitoringSensors.Where(x => x.Id == sensorA).FirstOrDefault();
            var sensorBObject = model.MonitoringSensors.Where(x => x.Id == sensorB).FirstOrDefault();
            var unit = sensor.Unit;
            this.result = "Difference  between sensors " + sensorAObject.Name + " and evaluated" + sensorBObject.Name + "is" + Math.Abs(valueA - Convert.ToDecimal(this.Evaluate(equation, Convert.ToString(valueB)).Value));
            return BaseEnum.AlarmTriggerType.ALARMRES_VIOLATION;
        }

        protected override string _GetErrors()
        {
            String errors = "";
            var model = new datacurrentEntities();
            var sensorIdsA = model.MonitoringSensorAlarms.Where(x => x.MonitoringStationAlarmConditionId == this.condIdx).Where(x => x.Active == true).Where(x => x.TypeA == true).Select(x => x.MonitoringSensor).ToList();
            var sensorIdsB = model.MonitoringSensorAlarms.Where(x => x.MonitoringStationAlarmConditionId == this.condIdx).Where(x => x.Active == true).Where(x => x.TypeB == true).Select(x => x.MonitoringSensor).ToList();
            var sensors = sensorIdsA.Union(sensorIdsB);
            string unit = "";
            MonitoringSensor unitsensor = null;
            List<MonitoringSensor> notValidSensor = new List<MonitoringSensor>();
            foreach (var sensor in sensors)
            {
                if (unit == "")
                {
                    unit = sensor.Unit;
                    unitsensor = sensor;
                }
                else
                {
                    if (sensor.Unit != unit)
                    {
                        notValidSensor.Add(sensor);
                    }

                }
            }
            if (notValidSensor.Count > 0)
                errors = "Sensor ";
            foreach (var sensor in notValidSensor)
            {
                errors = errors + sensor.Id + " " + sensor.Name;
            }
            if (notValidSensor.Count > 0)
                errors = errors + "does not have the same units as sensor " + unitsensor.Id + " " + unitsensor.Name;

            return errors;


        }

    }
}
