﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Civica.Domain.CivicaDomain;
using System.Data.Entity;
using Civica.Infrastructure.BaseClasses;

namespace Civica.BLL.Alarm
{
    public class TimeAvgMin : CondChecker
    {

        public TimeAvgMin(int condIdx) :
         base(condIdx)
        {
        }

        public override string CondType()
        {
            return "TIMEAVG_MIN";
        }

        /**
    * Trigger if the average of all values over the specified number of hours is 
    * less than the specified minimum
    * @return ALARMRES
    */
        public override BaseEnum.AlarmTriggerType _Check(DateTimeOffset testTime, DateTimeOffset testLastRun)
        {
            var model = new datacurrentEntities();
         
            var monitoringSensorIds = model.MonitoringSensorAlarms.Where(x => x.MonitoringStationAlarmConditionId == this.condIdx).Where(x => x.Active == true).Select(x => x.MonitoringSensorId);
            var monitoringSensors = model.MonitoringSensorAlarms.Where(x => x.MonitoringStationAlarmConditionId == this.condIdx).Where(x => x.Active == true).Select(x => x.MonitoringSensor).ToArray();
            var conditionparameter = model.MonitoringStationAlarmConditions.Where(x => x.Id == this.condIdx).Select(x => x.Params).FirstOrDefault();
            var averageHours = Convert.ToDouble(conditionparameter.Split(';')[2]);
            var averageValue = Convert.ToDecimal(conditionparameter.Split(';')[1]);
        
            var averagevalueList = new List<decimal>();
            var newStartTime = testTime;
            var newTestTime = testTime;
            while (DateTimeOffset.Compare(newStartTime, testLastRun) > 0)
            {
                newStartTime = newStartTime.AddHours((-1) * averageHours);
            }
            var data = model.MonitoringDatas.Where(x => monitoringSensorIds.Contains(x.MonitoringSensorId)).Where(x => x.TimeStamp <= testTime).Where(x => x.TimeStamp > newStartTime);
            if (data.Count() == 0)
            {
                // there are no rows
                // check if all the sensors are of data type "Point"
                bool points = true;
                foreach (var sensor in monitoringSensors)
                {

                    if (sensor.SensorDataTypeId != 1)
                    {
                        points = false;
                    }
                }
                if (points)
                {
                    if (averageValue > 0)
                    {
                        var sensor = model.MonitoringSensors.Where(x => x.Id == monitoringSensorIds.FirstOrDefault()).FirstOrDefault();
                        this.result = "Sensors:";
                        for (var j = 0; j < monitoringSensors.Count(); j++)
                        {
                            if (j != monitoringSensors.Count() - 1)
                            {
                                this.result = this.result + monitoringSensors[j].Name + "(" + monitoringSensors[j].Id + ")" + ", ";
                            }
                            else
                            {
                                this.result = this.result + monitoringSensors[j].Name + "(" + monitoringSensors[j].Id + ")";
                            }
                        }
                        // build the result
                        this.result = this.result + "\n" + "No New Data " + "From " + testLastRun.ToString() + " To " + testTime.ToString();
                        return BaseEnum.AlarmTriggerType.ALARMRES_VIOLATION;

                    }
                    else
                        return BaseEnum.AlarmTriggerType.ALARMRES_OK;
                }
                return BaseEnum.AlarmTriggerType.ALARMRES_NODATA;
            }
            if (averageValue > 0)
            {
                foreach (var monitoringsensor in monitoringSensors)
                {
                    if (monitoringsensor.SensorDataTypeId == 1)
                    {
                        var PointSensorData = data.Where(x => x.MonitoringSensorId == monitoringsensor.Id);
                        if (PointSensorData.Count() == 0)
                        {

                            this.result = this.result + monitoringsensor.Name + "(" + monitoringsensor.Id + ") " + "No New Data From" + testLastRun.ToString() + " To " + testTime.ToString();
                            return BaseEnum.AlarmTriggerType.ALARMRES_VIOLATION;
                        }
                    }
                }
            }
            var checkFromTimes = new List<DateTimeOffset>();
            var checkToTimes = new List<DateTimeOffset>();
            var dictonaryAverageValue = new Dictionary<int, List<decimal>>();
            int index = 0;
            while (newTestTime > testLastRun)
            {
                testTime = newTestTime;
                newTestTime = testTime.AddHours((-1) * averageHours);
                averagevalueList = data.Where(x => x.TimeStamp <= testTime).Where(x => x.TimeStamp > newTestTime).GroupBy(x => x.MonitoringSensorId).Select(x => new { sensorId = x.Key, averageValue = x.Average(row => row.Value) }).Select(x => x.averageValue).ToList();
                checkFromTimes.Add(newTestTime);
                checkToTimes.Add(testTime);
                dictonaryAverageValue.Add(index, averagevalueList);
                index++;
            }
            decimal VIOLATIONValue = 0;
            var allvalueok = true;
              DateTimeOffset? ViolationFromTime = null;
            DateTimeOffset? ViolationToTime = null;
         
            foreach (var key in dictonaryAverageValue)
            {
                var averages = dictonaryAverageValue[key.Key];
                foreach (var average in averages)
                {
                    if (average <= averageValue)
                    {
                        allvalueok = false;
                        VIOLATIONValue = average;
                        ViolationFromTime = checkFromTimes[key.Key];
                        ViolationToTime = checkToTimes[key.Key];
                        break;
                    }
                }
            }
          
            if (allvalueok)
            {
                return BaseEnum.AlarmTriggerType.ALARMRES_OK;
            }

            var Unitsensor = model.MonitoringSensors.FirstOrDefault();
            // build the result
            this.result = "Sensors:";
            for (var j = 0; j < monitoringSensors.Count(); j++)
            {
                if (j != monitoringSensors.Count() - 1)
                {
                    this.result = this.result + monitoringSensors[j].Name + "(" + monitoringSensors[j].Id + ")" + ", ";
                }
                else
                {
                    this.result = this.result + monitoringSensors[j].Name + "(" + monitoringSensors[j].Id + ")";
                }
            }
            // build the result
            this.result = this.result + "\n" + "From " + ViolationFromTime.Value.ToString() + " To " + ViolationToTime.Value.ToString() + " Avearge Value is " + VIOLATIONValue + " " + Unitsensor.Unit;
            return BaseEnum.AlarmTriggerType.ALARMRES_VIOLATION;
        }
        protected override string _GetErrors()
        {
            return "";
        }
    }
}
