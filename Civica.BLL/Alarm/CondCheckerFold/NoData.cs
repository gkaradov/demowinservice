﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Civica.Domain.CivicaDomain;
using System.Data.Entity;
using Civica.Infrastructure.BaseClasses;

namespace Civica.BLL.Alarm
{
    public class NoData : CondChecker
    {
        public NoData(int condIdx) :
         base(condIdx)
        {
        }

        public override string CondType()
        {
            return "NO_DATA";
        }
        public override BaseEnum.AlarmTriggerType _Check(DateTimeOffset testTime, DateTimeOffset testLastRun)
        {
            var model = new datacurrentEntities();
            var stationId = model.MonitoringStationAlarmConditions.Where(x => x.Id == this.condIdx).FirstOrDefault().MonitoringStationAlarm.MonitoringStationId;
            var stationTimeOffSet = model.MonitoringStations.Where(x => x.Id == stationId).FirstOrDefault().TimeZone.Offset;
            var stationTimeZoneSpan = new TimeSpan(Convert.ToInt32(stationTimeOffSet), 0, 0);
            var monitoringSensorIds = model.MonitoringSensorAlarms.Where(x => x.MonitoringStationAlarmConditionId == this.condIdx).Where(x => x.Active == true).Select(x => x.MonitoringSensorId);
            var conditionparameter = model.MonitoringStationAlarmConditions.Where(x => x.Id == this.condIdx).Select(x => x.Params).FirstOrDefault();
            var timebuffer = Convert.ToDouble(conditionparameter.Split(';')[1]);
            var newTestTime = testTime.AddHours((-1) * timebuffer);
            var sensorIds = model.MonitoringDatas.Where(x => monitoringSensorIds.Contains(x.MonitoringSensorId)).Where(x => x.TimeStamp <= testTime).Where(x => x.TimeStamp > newTestTime).Select(x => x.MonitoringSensorId).Distinct();
            var badSensors = new List<int>();
            foreach (var senesorId in monitoringSensorIds)
            {
                badSensors.Add(senesorId);
            }
            var i = 0;
            
            foreach (var sensorId in sensorIds)
            {
                if (badSensors.Contains(sensorId))
                {
                    badSensors.RemoveAt(i);
                }
                i++;

            }
            if (badSensors.Count() == 0)
                return BaseEnum.AlarmTriggerType.ALARMRES_OK;


            // no data in the last X hours, so there *is* a violation

            // find out when we last saw data
            var dataList = model.MonitoringDatas.Where(x => badSensors.Contains(x.MonitoringSensorId)).Where(x => x.TimeStamp <= testTime).GroupBy(x => x.MonitoringSensorId).Select(x => new { sensorId = x.Key, TimeStamp = x.Max(row => row.TimeStamp)});

            var tempSensorIdList = new List<int>();
            foreach (var data in dataList)
            {
                var lastTime = data.TimeStamp;
                var sensor = model.MonitoringSensors.Where(x => x.Id == data.sensorId).FirstOrDefault();
                this.result = result + "Sensor:" + sensor.Name + "(" + sensor.Id + ")" +"\n" + "Data last seen = " + lastTime.ToOffset(stationTimeZoneSpan).DateTime + "\n";
                tempSensorIdList.Add(data.sensorId);

            }
            foreach (var badSensor in badSensors)
            {
                if (!tempSensorIdList.Contains(badSensor))
                {
                    var sensor = model.MonitoringSensors.Where(x => x.Id == badSensor).FirstOrDefault();
                    this.result = result + "Sensor:" + sensor.Name + "(" + sensor.Id + ")"+"\n" + "Data last seen =  never" + "\n" ;
                }
            }
            return BaseEnum.AlarmTriggerType.ALARMRES_VIOLATION;
        }
        protected override string _GetErrors()
        {
            return "";
        }
    }
}
