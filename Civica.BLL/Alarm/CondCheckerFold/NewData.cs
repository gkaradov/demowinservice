﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Civica.Domain.CivicaDomain;
using System.Data.Entity;
using Civica.Infrastructure.BaseClasses;

namespace Civica.BLL.Alarm
{
    public class NewData : CondChecker
    {

        public NewData(int condIdx) :
         base(condIdx)
        {
        }


        public override string CondType()
        {
            return "Max";
        }
        /**
         * Trigger if there are any values (since we last checked) which are 
         * greater than the threshold value.
         * @return ALARMRES
        */
        public override BaseEnum.AlarmTriggerType _Check(DateTimeOffset testTime, DateTimeOffset testLastRun)
        {
            var model = new datacurrentEntities();
           
            var monitoringSensorIds = model.MonitoringSensorAlarms.Where(x => x.MonitoringStationAlarmConditionId == this.condIdx).Where(x => x.Active == true).Select(x => x.MonitoringSensorId);
            var conditionparameter = model.MonitoringStationAlarmConditions.Where(x => x.Id == this.condIdx).Select(x => x.Params).FirstOrDefault();
            var timebuffer = Convert.ToDouble(conditionparameter.Split(';')[1]);
            var newTestLastRun = testTime.AddHours((-1) * timebuffer);

            var dataList = model.MonitoringDatas.Where(x => monitoringSensorIds.Contains(x.MonitoringSensorId)).Where(x => x.TimeStamp <= testTime).Where(x => x.TimeStamp > newTestLastRun);
            if (dataList.Count() == 0)
            {
                return BaseEnum.AlarmTriggerType.ALARMRES_OK;
            }
            this.result = "SensorId =" + String.Join(",", monitoringSensorIds) + "\n" + "Number of data points seen = " + dataList.Count();
            return BaseEnum.AlarmTriggerType.ALARMRES_VIOLATION;
        }
        protected override string _GetErrors()
        {
            return "";
        }
    }
}
