﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Civica.Domain.CivicaDomain;

namespace Civica.BLL.Models
{
    public class SensorEquationDTO
    {

        public SensorEquationDTO()
        {
           // this.LoggerChannel = new  SensorLoggerChannelDTO();
        }

        public int Id { get; set; }
        public int SensorId { get; set; }
        public Nullable<System.DateTimeOffset> ValidUntil { get; set; }
        public string Equation { get; set; }
        public Nullable<int> MonitoringLoggerChannelId { get; set; }
    


        public SensorLoggerChannelDTO  LoggerChannel { get; set; }



        public static SensorEquationDTO Create(MonitoringSensorEquation equation)
        {
            SensorEquationDTO dto = new SensorEquationDTO();

            dto.Id = equation.Id;
            dto.SensorId = equation.SensorId;
            dto.ValidUntil = equation.ValidUntil;
            dto.Equation = equation.Equation;
            dto.MonitoringLoggerChannelId = equation.MonitoringLoggerChannelId;

            if(!equation.MonitoringSensor.Calculated)
            {
                if(equation.MonitoringLoggerChannel != null)
                {
                    dto.LoggerChannel = SensorLoggerChannelDTO.Create(equation.MonitoringLoggerChannel);
                }
            }

            

            return dto;
        }

    }


    public class SensorLoggerChannelDTO
    {
        public int Id { get; set; }
        public int LoggerTypeId { get; set; }
        public string Channel { get; set; }
        public string Code { get; set; }

        public static SensorLoggerChannelDTO Create(MonitoringLoggerChannel channel)
        {
            SensorLoggerChannelDTO dto = new SensorLoggerChannelDTO();

            dto.Id = channel.Id;
            dto.LoggerTypeId = channel.LoggerTypeId;
            dto.Channel = channel.Channel;
            dto.Code = channel.Code;

            return dto;
        }


        }

    }