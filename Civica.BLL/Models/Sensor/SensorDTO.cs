﻿using System;
using System.Collections.Generic;
using System.Linq;
using Civica.Domain.CivicaDomain;


namespace Civica.BLL.Models
{
    public class SensorDTO
    {
        public SensorDTO()
        {
            this.Equations = new List<SensorEquationDTO>();
            this.CurrentEquation = new SensorEquationDTO();

        }

        public int Id { get; set; }
        public int MonitorStationId { get; set; }
        public int SensorTypeId { get; set; }
        public string SensorType { get; set; }
        public int SensorDataTypeId { get; set; }
        public int SensorStatusId { get; set; }

        public string Name { get; set; }
        public string ShortName { get; set; }

        public Nullable<decimal> TimeInterval { get; set; }

        public string TriggerChannel { get; set; }
        public string TriggerCondition { get; set; }
        public bool Calculated { get; set; }
        public Nullable<int> TriggerResetDelay { get; set; }

        public Nullable<int> OffSiteSensorId { get; set; }

        public List<SensorEquationDTO> Equations { get; set; }


        public SensorEquationDTO CurrentEquation { get; set; }

        public static SensorDTO Create(MonitoringSensor sensor)
        {
            SensorDTO dto = new SensorDTO();

            dto.Id = sensor.Id;
            dto.MonitorStationId = sensor.MonitoringStation.Id;
            dto.SensorTypeId = sensor.MonitoringSensorType.Id;
            dto.SensorType = sensor.MonitoringSensorType.Type;

            dto.SensorDataTypeId = sensor.MonitoringSensorDataType.Id;
            dto.SensorStatusId = sensor.MonitoringSensorStatu.Id;

            dto.Name = sensor.Name;
            dto.ShortName = sensor.ShortName;

            dto.TimeInterval = sensor.TimeInterval;
            dto.TriggerChannel = sensor.TriggerChannel;
            dto.TriggerResetDelay = sensor.TriggerResetDelay;
            dto.OffSiteSensorId = sensor.OffSiteSensorId;
            dto.Calculated = sensor.Calculated;


            foreach (var se in sensor.MonitoringSensorEquations.Where(x => x.Active == true))
            {
                dto.Equations.Add(SensorEquationDTO.Create(se));
            }


            var fe = sensor.MonitoringSensorEquations.Where(p => p.ValidUntil == null).Where(x => x.Active == true).FirstOrDefault();


            var ce = sensor.MonitoringSensorEquations.Where(x => x.Active == true).Where(x => x.ValidUntil != null && DateTimeOffset.Parse(x.ValidUntil.ToString()).UtcDateTime >= DateTimeOffset.UtcNow ).OrderBy(x => x.ValidUntil).FirstOrDefault();

            if (ce != null)
            {
                dto.CurrentEquation = SensorEquationDTO.Create(ce);
            }
            else
            {
                if(fe != null)
                {
                    dto.CurrentEquation = SensorEquationDTO.Create(fe);
                }
            }


            return dto;
        }




    }
}
