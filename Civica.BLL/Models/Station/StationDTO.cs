﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Civica.Infrastructure.BaseClasses;
using Civica.Domain.CivicaDomain;
using System.Data;

namespace Civica.BLL.Models
{
    public class StationDTO
    {
        public StationDTO()
        {
            this.Sensors = new List<SensorDTO>();
            this.Logger = new LoggerDTO();
        }

        public int Id { get; set; }
        public int TypeId { get; set; }
        public int? StateId { get; set; }
        public int? MonitoringLoggerId { get; set; }
        public int TimeZoneId { get; set; }
        public string Name { get; set; }
        public DateTimeOffset? StartDate { get; set; }
        public DateTimeOffset? EndDate { get; set; }
        public bool Active { get; set; }
        public LoggerDTO Logger { get; set; }
        public List<SensorDTO> Sensors { get; set; }
        public TimeSpan TimeZoneOffset { get; set; }
        public DateTimeOffset? LastTimeStamp { get; set; }
        public DateTimeOffset? LastTimeStampAdded { get; set; }

        public static StationDTO Create(MonitoringStation station)
        {
            StationDTO dto = new StationDTO();

            dto.Id = station.Id;
            dto.TypeId = station.TypeId;
            dto.StateId = station.StateId;
            dto.MonitoringLoggerId = station.MonitoringLoggerId;
            dto.TimeZoneId = station.TimeZoneId;
            dto.Name = station.Name;

            dto.StartDate = station.StartDate;
            dto.EndDate = station.EndDate;
            dto.Active = station.Active;
            dto.TimeZoneOffset = new TimeSpan((int)station.TimeZone.Offset, 0, 0);
            dto.LastTimeStamp = station.LastTimeStamp;
            dto.LastTimeStampAdded = station.LastTimeStampAdded;

            if (station.MonitoringLogger != null)
            {
                dto.Logger = LoggerDTO.Create(station.MonitoringLogger);
            }

            var sensors = station.MonitoringSensors.Where(x => x.Calculated != true).ToList();
            foreach (var se in station.MonitoringSensors)
            {
                if(!se.Calculated)
                {
                    dto.Sensors.Add(SensorDTO.Create(se));
                }   
            }

            return dto;
        }

    }


    public class CalculatedSensorsDependencyNode
    {
        public CalculatedSensorsDependencyNode()
        {
            this.Node = new  SensorDTO();
            this.Children = new List<CalculatedSensorsDependencyNode>();
            this. ParentIds = new List<int>();
        }

        public int Level { get; set; }
        public bool IsLeaf { get; set; }
        public  SensorDTO Node { get; set; }
        public List<CalculatedSensorsDependencyNode> Children { get; set; }
        public List<int> ParentIds { get; set; }
    }


    public class CalculatedSensorMap
    {
        public CalculatedSensorMap()
        {
            this.Sensors = new List<SensorDTO>();
            this.TimeSeries = new List<DateTimeOffset>();
        }

        public List<SensorDTO> Sensors { get; set; }
        public List<DateTimeOffset> TimeSeries { get; set; }
    }


    public class SensorMap
    {
        public SensorMap()
        {
            this.table = new DataTable();
            table.Columns.Add("MonitoringSensorId", typeof(int));
            table.Columns.Add("Value", typeof(double));
            table.Columns.Add("TimeStamp", typeof(DateTimeOffset));
            table.Columns.Add("Processed", typeof(int));

            this.tableraw = new DataTable();
            tableraw.Columns.Add("MonitoringSensorId", typeof(int));
            tableraw.Columns.Add("Value", typeof(double));
            tableraw.Columns.Add("TimeStamp", typeof(DateTimeOffset));

        }

        public int Position { get; set; }
        public SensorDTO Sensor { get; set; }
        public String Chanel { get; set; }
        public DateTimeOffset? LasTimeStamp { get; set; }
        public DataTable table = new DataTable();
        public DataTable tableraw = new DataTable();

    }

    public class Data
    {
        public double value;
        public DateTime timestamp;
    }


    public class UndoMap
    {
        public UndoMap()
        {
            //this.Table = new SensorDataOffsets ();
        }

        public SensorDTO Sensor { get; set; }
        public DateTimeOffset BeginTimeStamp { get; set; }
        public DateTimeOffset EndTimeStamp { get; set; }
        public  SensorDataOffsets[] Table   { get; set; }
}

}
