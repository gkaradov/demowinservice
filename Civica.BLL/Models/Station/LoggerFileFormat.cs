﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Civica.BLL.Models.Station
{

    //  DTO stands for Data Transfer Object

    public class LoggerPacketDTO
    {



        public LoggerPacketDTO()  //initiating any complex object parameters
        {
            this.Chanels = new List<ChanelDTO>();
            this.Alarms = new List<AlarmDTO>();
            // add anything else that comes to mind and is a complex object

        }

        //Simple Parameters
        public int Id { get; set; }
        public int TypeId { get; set; }
        public string Name { get; set; }
        public string SerialNumber { get; set; }   //decide if you want it as a string or as a number

        //Use one or both of these fields to anounse where in the world is the sensor.  A sensor set up can specify the timne zone, and then the data is send as Universal Greenwitch Time. 
        //No need of countless conversions on the back end.  Can sell/use the sensor anywhere in the world.

        public int TimeZoneID { get; set; }
        //or
        public string TimeZoneCode { get; set; }


        // ......   any other simple parameters that you may need


        //Complex parameter objects
        public List<ChanelDTO> Chanels { get; set; }
        public List<AlarmDTO> Alarms { get; set; }

        // ......   any other complex parameters that you may need

    }

    public class AlarmDTO
    {
        public int Id { get; set; }
        public int TypeId { get; set; }
        public string Name { get; set; }

        //......  anything esle that may be needed


    }


    public class ChanelDTO
    {
        public ChanelDTO()
        {
            this.Values = new List<ValueDTO>();
            // initiate other complex objects if needed
        }

        //Simple Parameters
        public int Id { get; set; }
        public int TypeId { get; set; }
        public string Name { get; set; }

        // ......   any other simple parameters that you may need


        //Complex parameter objects
        public List<ValueDTO> Values { get; set; }

        // ......   any other complex parameters that you may need
    }



    public class ValueDTO
    {
        public decimal Value { get; set; }
        public DateTimeOffset TimeStamp { get; set; }

    }


}
