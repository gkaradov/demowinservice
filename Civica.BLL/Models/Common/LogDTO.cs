﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Civica.BLL.Models
{
    public class LogDTO
    {
        public LogDTO()
        {

        }

        public int Id { get; set; }
        public int? StationId { get; set; }
        public string FileMane { get; set; }
        public string UniqueId { get; set; }
        public string ErrorMessage { get; set; }
        public string Stack { get; set; }
        public System.DateTimeOffset TimeStamp { get; set; }
    
    }

}

