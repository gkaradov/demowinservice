﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Civica.Infrastructure.BaseClasses;
using Civica.BLL.Models;

namespace Civica.BLL.CommonClasses.Interfaces
{
    public interface  IDetecDAT
    {
        ServiceResult<bool> GetFileData(FileResult file, MonitoringTaskDTO task);

    }
}
