﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Civica.BLL.Models;

namespace Civica.BLL.CommonClasses.Interfaces
{
    public interface IDataProcessing
    {
        double? EvaluateMSensor(string equation, string channel, double value);
        bool BulckUploadMSensors(List<SensorMap> map);
        bool BulckUploadCsensors(CalculatedSensorMap sensormao);
    }
}
