﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Civica.Infrastructure.BaseClasses;
using Civica.BLL.Models;

namespace Civica.BLL.CommonClasses.Interfaces
{
    public interface ICivicaCSV
    {

        ServiceResult<bool> GetFileData(FileResult file,  MonitoringTaskDTO task);
    }



}
