﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Collections;
using Civica.BLL.CommonClasses.Interfaces;
using System.Text.RegularExpressions;
using Civica.Infrastructure.BaseClasses;
using Civica.BLL.Models;
using System.Configuration;
using System.Data.SqlClient;
using Civica.DBL.DBClasses;
using System.Threading;
using System.Threading.Tasks;
using Civica.DBL.SPModels;

namespace Civica.BLL.CommonClasses
{
    class DataProcessing //: IDataProcessing
    {

        public double? EvaluateMSensor(string equation, string channel, double value)
        {

            var regexp = new Regex("(^|[^A-Za-z_])m\\(" + channel + "\\)", RegexOptions.IgnoreCase);
            var eq = regexp.Replace(equation, "$1 " + (decimal)value);
            var res = (new Calculation()).evaluate(eq);

            return res;
        }


        public double? EvaluateEquation(string equation)
        {
            return (new Calculation()).evaluate(equation);
        }

        public double? EvaluateCalculatedSensor(string equation, List<SensorDataSP> sd)
        {
            try
            {
                if (sd != null)
                {
                    if (sd.Count > 0)
                    {
                        foreach (var s in sd)
                        {
                            var regexp = new Regex("(^|[^A-Za-z_])s\\(" + s.SensorId.ToString() + "\\)");
                            equation = regexp.Replace(equation, "$1 " + s.Value);
                        }

                        if (!equation.Contains("s"))
                        {
                            return (new Calculation()).evaluate(equation);
                        }
                    }
                }
                return null;
            }
            catch
            {
                return null;
            }
           
        }

        public ServiceResult<bool> BulckUploadPhysicalSensors(List<SensorMap> map)
        {
            var res = new ServiceResult<bool>(true);
            DateTimeOffset st;
            DateTimeOffset en;
            try
            {
                foreach (var sensor in map)
                {

                    if (sensor.table.Rows.Count > 0)
                    {
                        var t = sensor.table.Select().OrderBy(u => u["TimeStamp"]).ToArray();
                        st = (DateTimeOffset)t[0]["TimeStamp"];
                        en = (DateTimeOffset)t[t.Length - 1]["TimeStamp"];
                        var r = SensorDBMethods.DeleteTimeRange(sensor.Sensor.Id, st, en);
                        if (!r.DTO)
                        {
                            res.DTO = r.DTO;
                            foreach (var m in r.ErrorResults)
                            {
                                res.AddError(m.Key, m.Value);
                            }
                            return res;
                        }
                        res = BulkUpload(sensor.table);
                    }

                    if (sensor.tableraw.Rows.Count > 0)
                    {
                        var t = sensor.tableraw.Select().OrderBy(u => u["TimeStamp"]).ToArray();
                        st = (DateTimeOffset)t[0]["TimeStamp"];
                        en = (DateTimeOffset)t[t.Length - 1]["TimeStamp"];
                        var r = SensorDBMethods.DeleteTimeRangeRaw(sensor.Sensor.Id, st, en);
                        if (!r.DTO)
                        {
                            res.DTO = r.DTO;
                            foreach (var m in r.ErrorResults)
                            {
                                res.AddError(m.Key, m.Value);
                            }
                            return res;
                        }
                        res = BulkUploadRaw(sensor.tableraw);
                    }
                }

            }
            catch (Exception ex)
            {
                res.DTO = false;
                res.AddError("System Error", ex.Message);
            }
           
            return res;
        }

        public ServiceResult<bool> BulckUploadCalculatedsensors(CalculatedSensorMap sensormap)
        {

            var result = new ServiceResult<bool>(true);
            Hashtable executionOrder = new Hashtable();
            var sensorTree = CreateDependencyTree(sensormap.Sensors, 0, 0, ref executionOrder);
            var levels = executionOrder.Values.Cast<CalculatedSensorsDependencyNode>().Select(x => x.Level).Distinct().ToList();

            if (levels != null)
            {
                levels.Sort();

                if (levels.Count() > 1)
                {
                    for (var i = 1; i < levels.Count(); i++)
                    {
                        var levelColection = executionOrder.Values.Cast<CalculatedSensorsDependencyNode>().Where(x => x.Level == levels[i]);

                        foreach (var sensor in levelColection)
                        {
                            var t = ProcessCalculatedSensor(sensor, sensormap.TimeSeries);

                            if( t.ErrorResults.Count() > 0)
                            {
                                foreach(var e in t.ErrorResults)
                                {
                                    result.ErrorResults.Add(e);
                                }
                            }

                            if (t.ServiceLogs.Count() > 0)
                            {
                                foreach (var s in t.ServiceLogs)
                                {
                                    result.ServiceLogs.Add(s);
                                }
                            }
                        }

                        //Parallel.ForEach(levelColection, (sensor) =>
                        //{
                        //    var t = ProcessCalculatedSensor(sensor, sensormap.TimeSeries);


                        //    if (t.ErrorResults.Count() > 0)
                        //    {
                        //        foreach (var e in t.ErrorResults)
                        //        {
                        //            result.ErrorResults.Add(e);
                        //        }
                        //    }

                        //    if (t.ServiceLogs.Count() > 0)
                        //    {
                        //        foreach (var s in t.ServiceLogs)
                        //        {
                        //            result.ServiceLogs.Add(s);
                        //        }
                        //    }
                        //});

                    }
                }
            }

            return result; ;
        }


        private ServiceResult<bool> ProcessCalculatedSensor(CalculatedSensorsDependencyNode node, List<DateTimeOffset> timeseries)
        {
            int begin = 0;
           // int end = 0;

            var res = new ServiceResult<bool>(true);

            List<DateTimeOffset> timeblock = new List<DateTimeOffset>();
     
            while (begin <= timeseries.Count() - 1)
            {
                if ((begin + 10000) < timeseries.Count())
                {
                    timeblock = timeseries.GetRange(begin, 10000);
                }
                else
                {
                    timeblock = timeseries.GetRange(begin, timeseries.Count() - begin);
                }

                if (timeblock.Count() > 0)
                {
                    res = ProcessCalculatedSensorBlock(node, timeblock);
                }
                begin = begin + 10000;
            }

            return res;
        }

        private ServiceResult<bool> ProcessCalculatedSensorBlock(CalculatedSensorsDependencyNode node, List<DateTimeOffset> timeblock)
        {
            double value = 0;
            var start = timeblock.ElementAt(0);
            var smapList = new List<SensorMap>();
            var ret = new ServiceResult<bool>(true);

            Match m;
            Regex gen_regexp = new Regex("(?:^|[^A-Za-z_])(s\\([0-9]+\\))");
            Regex gen_regexp_delay = new Regex("(?:^|[^A-Za-z_])(delay\\((s\\([0-9]+\\))(,[ ]*-*[0-9]+)\\))");
            var regularPatern = "(?:^|[^A-Za-z_])(s\\([0-9]+\\))";
            var delayPatern = "(?:^|[^A-Za-z_])(delay\\((s\\([0-9]+\\))(,[ ]*-*[0-9]+)\\))";

            var table = new DataTable();
            table.Columns.Add("MonitoringSensorId", typeof(int));
            table.Columns.Add("Value", typeof(double));
            table.Columns.Add("TimeStamp", typeof(DateTimeOffset));
            table.Columns.Add("Processed", typeof(int));

            try
            {

                var res = SensorDBMethods.DeleteTimeRange(node.Node.Id, timeblock.ElementAt(0), timeblock.ElementAt(timeblock.Count() - 1));
                var tempEquation = node.Node.CurrentEquation.Equation;

                //"if (lt(s(2783) - delay(s(2778), 15), 0),0,s(2783) - delay(s(2778), 15))" - sample equation for future refference
                //This should be a typical match - "delay(s(2778), 15)"

                //Delayed Sensor matches - find delay sensor matches, extract the delay sensor ID and delay offset, 
                //and create a new sensor object for passing to the DB.  
                //Disregard similar matches, as we will retrive only one value for a given unique match
                //For the sample equation above, we will have only one entry in sensors for delay(s(2778), 15)

                MatchCollection delayMatches = Regex.Matches(tempEquation, delayPatern);

                if (delayMatches != null)
                {
                    if (delayMatches.Count > 0)
                    {
                        var sensors = new List<DelayedSensorWithOffset>();
                        double offsetValue = 0;
                        int idValue = 0;

                        foreach (Match match in delayMatches)
                        {

                            if (match.Value != "") // The match engine produces some funky empty matches.  For reference check this URL: msdn.microsoft.com/en-us/library/system.text.regularexpressions.regex.matches%28v=vs.110%29.aspx
                            {
                                if (sensors.Where(x => x.ReplaceString == match.Value).Count() == 0)
                                {
                                    var sensor = new DelayedSensorWithOffset();

                                    //Typical delay match :  delay(s(2778), 15).  Will split in two, by "," delimiter
                                    var parts = match.Value.Split(',');
                                    if (parts.Length == 2)
                                    {
                                        var offset = parts[1].Replace(")", "");

                                        var id = parts[0].Replace(")", "");
                                        id = id.Replace("delay(s(", "");

                                        if (Int32.TryParse(offset, out idValue))
                                        {
                                            sensor.Id = idValue;
                                        }
                                        else
                                        {
                                            ret.DTO = false;
                                            return ret;
                                        }

                                        if (Double.TryParse(offset, out offsetValue))
                                        {
                                            sensor.Offset = offsetValue;
                                        }
                                        else
                                        {
                                            ret.DTO = false;
                                            return ret;
                                        }

                                        if (Double.TryParse(offset, out offsetValue))
                                        {
                                            sensor.Offset = offsetValue;
                                        }
                                        else
                                        {
                                            ret.DTO = false;
                                            return ret;
                                        }
                                    }
                                    else
                                    {
                                        ret.DTO = false;
                                        return ret;
                                    }

                                    sensors.Add(sensor);
                                    tempEquation = tempEquation.Replace(match.Value, "");
                                }

                            }
                        }

                        // now we find all non delayed sensor matches.  By now the sensor equation should look like this:

                        //"if (lt(s(2783) -, 0),0,s(2783) - )"

                        MatchCollection sensorMatches = Regex.Matches(tempEquation, regularPatern);
                        foreach (Match match in sensorMatches)
                        {
                            if (match.Value != "") // The match engine produces some funky empty matches.  For reference check this URL: msdn.microsoft.com/en-us/library/system.text.regularexpressions.regex.matches%28v=vs.110%29.aspx
                            {

                                if (sensors.Where(x => x.ReplaceString == match.Value).Count() == 0)
                                {
                                    var sensor = new DelayedSensorWithOffset();

                                    sensor.Offset = 0;
                                    sensor.ReplaceString = match.Value;

                                    var id = match.Value.Replace(")", "");
                                    id = id.Replace("s(", "");

                                    if (Int32.TryParse(id, out idValue))
                                    {
                                        sensor.Id = idValue;
                                    }
                                    else
                                    {
                                        ret.DTO = false;
                                        return ret;
                                    }

                                    sensors.Add(sensor);
                                    tempEquation = tempEquation.Replace(match.Value, "");

                                }
                            }

                        }

                        var sensordata = SensorDBMethods.GetParentDelayedSensorData(sensors, timeblock);

                        foreach (var ts in timeblock)
                        {
                            var tsValues = sensordata.Where(x => x.Timestamp == ts).ToList();

                            if (tsValues != null)
                            {
                                if (tsValues.Count() > 0)
                                {

                                    var eq = node.Node.CurrentEquation.Equation;

                                    foreach (var tsv in tsValues)
                                    {
                                        eq = eq.Replace(tsv.ReplacmentString, tsv.Value.ToString());
                                    }
                                    var calculated = EvaluateEquation(eq);
                                    if (calculated != null)
                                    {
                                        value = (double)calculated;
                                        table.Rows.Add(node.Node.Id, value, ts, 0);
                                    }
                                    else
                                    {
                                        if (!(ret.ServiceLogs.Count > 0))
                                        {
                                            ret.AddServiceLog("Warning for node " + node.Node.Id, "At least one calculated values was returned as NaN");
                                        }
                                    }
                                    
                                }
                            }
                        }
                    }
                    else
                    {
                        var sensordata = SensorDBMethods.GetParentSensorData(node.ParentIds, timeblock);

                        if (sensordata != null)
                        {
                            foreach (var ts in timeblock)
                            { 
                                var tsValues = sensordata.Where(x => x.Timestamp == ts).Distinct().ToList();
                                if (tsValues != null)
                                {
                                    if (tsValues.Count() > 0)
                                    {
                                        string equation = FindCurrentEquation(node.Node, ts);

                                       

                                        var calculated = EvaluateCalculatedSensor(equation, tsValues);

                                        if (calculated != null)
                                        {
                                            value = (double)calculated;
                                            table.Rows.Add(node.Node.Id, value, ts, 0);
                                        }
                                        else
                                        {
                                            if (!(ret.ServiceLogs.Count > 0))
                                            {
                                                ret.AddServiceLog("Warning for node " + node.Node.Id, "At least one calculated values was returned as NaN");
                                            }
                                        }

                                        
                                    }        
                                }
                            }
                        }
                    }
                }
                else
                {
                    var sensordata = SensorDBMethods.GetParentSensorData(node.ParentIds, timeblock);
                    if (sensordata != null)
                    {
                        foreach (var ts in timeblock)
                        {
                            var tsValues = sensordata.Where(x => x.Timestamp == ts).ToList();
                            if (tsValues != null)
                            {
                                var calculated = EvaluateCalculatedSensor(node.Node.CurrentEquation.Equation, tsValues);
                                if (calculated != null)
                                {
                                    value = (double)calculated;
                                    table.Rows.Add(node.Node.Id, value, ts, 0);
                                }
                                else
                                {
                                    if(!(ret.ServiceLogs.Count > 0))
                                    {
                                        ret.AddServiceLog("Warning for node " + node.Node.Id, "At least one calculated values was returned as NaN");
                                    }    
                                }
                                
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
                ret.AddError("Error for node " + node.Node.Id, e.Message);
                ret.DTO = false;
            }

            if (table.Rows.Count > 0)
            {
                var t = BulkUpload(table);
            }


            return ret;
        }



        public string FindCurrentEquation(SensorDTO node, DateTimeOffset ts)
        {
            string eq = node.CurrentEquation.Equation;
            DateTimeOffset? basetime = null;
            DateTimeOffset previous;

            foreach (var e in node.Equations)
            {
                if (e.ValidUntil != null)
                {
                    if (e.ValidUntil < ts)
                    {                     
                        continue;
                    }
                    else
                    {
                        if (basetime == null)
                        {
                            eq = e.Equation;
                            basetime = e.ValidUntil;
                        }
                        else
                        {
                            if (basetime > e.ValidUntil)
                            {
                                eq = e.Equation;
                                basetime = e.ValidUntil;
                            }
                        }
                       
                    }
                }
                else
                {
                   
                    continue;
                }


                previous = ts;
            }

            return eq;
        }



        private ServiceResult<bool> BulkUpload(DataTable table)
        {
            var res = new ServiceResult<bool>(true);

            string BIConString = ConfigurationManager.ConnectionStrings["LocalSQL"].ConnectionString;

            try
            {
                using (SqlConnection dbConnection = new SqlConnection(BIConString))
                {
                    dbConnection.Open();
                    // Instantiate SqlBulkCopy and configure Bulkcopy propoerties
                    var transaction = dbConnection.BeginTransaction();
                    using (SqlBulkCopy s = new SqlBulkCopy(dbConnection, SqlBulkCopyOptions.TableLock, transaction))
                    {
                        s.BatchSize = 2000;
                        s.NotifyAfter = 2000;
                        s.BulkCopyTimeout = 0;
                        s.DestinationTableName = "MonitoringData";

                        s.ColumnMappings.Add("MonitoringSensorId", "MonitoringSensorId");
                        s.ColumnMappings.Add("Value", "Value");
                        s.ColumnMappings.Add("TimeStamp", "TimeStamp");
                        s.ColumnMappings.Add("Processed", "Processed");
                        s.WriteToServer(table);
                        transaction.Commit();
                        s.ColumnMappings.Clear();
                    }
                    dbConnection.Close();
                    dbConnection.Dispose();
                }
            }
            catch (Exception ex)
            {
                res.ErrorResults.Add("Physical Sensors Bulk Upload - ErrorMessage", "Error Message: " + ex.Message);
                if (ex.InnerException != null)
                {
                    res.ErrorResults.Add("Physical Sensors Bulk Upload - Inner Exception", "Inner Exception : " + ex.InnerException);
                }
                res.ErrorResults.Add("Physical Sensors Bulk Upload - Stack Trace", "Stack Trace : " + ex.StackTrace);
                res.DTO = false;
            }

            return res;
        }

        private ServiceResult<bool> BulkUploadRaw(DataTable table)
        {
            var res = new ServiceResult<bool>(true);
            string BIConString = ConfigurationManager.ConnectionStrings["LocalSQL"].ConnectionString;

            try
            {
                using (SqlConnection dbConnection = new SqlConnection(BIConString))
                {
                    dbConnection.Open();
                    // Instantiate SqlBulkCopy and configure Bulkcopy propoerties
                    var transaction = dbConnection.BeginTransaction();
                    using (SqlBulkCopy s = new SqlBulkCopy(dbConnection, SqlBulkCopyOptions.TableLock, transaction))
                    {
                        s.BatchSize = 2000;
                        s.NotifyAfter = 2000;
                        s.BulkCopyTimeout = 0;
                        s.DestinationTableName = "MonitoringDataRaw";

                        s.ColumnMappings.Add("MonitoringSensorId", "MonitoringSensorId"); s.ColumnMappings.Add("Value", "Value"); s.ColumnMappings.Add("TimeStamp", "Timestamp");
                        s.WriteToServer(table);
                        transaction.Commit();
                        s.ColumnMappings.Clear();
                    }
                    dbConnection.Close();
                    dbConnection.Dispose();
                }
            }
            catch (Exception ex)
            {
                res.ErrorResults.Add("Physical Sensors Bulk Upload", "Error Message: " + ex.Message);
                if (ex.InnerException != null)
                {
                    res.ErrorResults.Add("Physical Sensors Bulk Upload", "Inner Exception : " + ex.InnerException);
                }
                res.ErrorResults.Add("Physical Sensors Bulk Upload", "Stack Trace : " + ex.StackTrace);
                res.DTO = false;
            }
            return res;
        }


        private List<CalculatedSensorsDependencyNode> CreateDependencyTree(List<SensorDTO> sensors, int parentId, int level, ref Hashtable exOrder)
        {

            var tree = new List<CalculatedSensorsDependencyNode>();

            level += 1;
            foreach (var sensor in sensors)
            {

                var node = new CalculatedSensorsDependencyNode();
                node.Node = sensor;
                node.Level = level;

                if (parentId > 0)
                {
                    var parents = SensorDBMethods.GetSensorParents(sensor.Id);

                    if (parents != null)
                    {

                        if (parents.Count() > 0)
                        {
                            foreach (var p in parents)
                            {
                                node.ParentIds.Add(p);
                            }
                        }
                        else
                        {
                            node.ParentIds.Add(parentId);
                        }
                    }
                    else
                    {
                        node.ParentIds.Add(parentId);
                    }
                }

                var children = SensorDBMethods.GetSensorDependencies(sensor.Id);

                if (children != null)
                {
                    if (children.Count() > 0)
                    {
                        node.IsLeaf = false;
                        var childSensors = new List<SensorDTO>();
                        foreach (var child in children)
                        {
                            childSensors.Add(SensorDTO.Create(child));
                        }

                        var childNodes = CreateDependencyTree(childSensors, sensor.Id, level, ref exOrder);

                        foreach (var cNode in childNodes)
                        {
                            node.Children.Add(cNode);
                        }
                    }
                    else
                    {
                        node.IsLeaf = true;
                    }
                }
                else
                {
                    node.IsLeaf = true;
                }

                if (exOrder.ContainsKey(node.Node.Id))
                {
                    CalculatedSensorsDependencyNode itemInhash = (CalculatedSensorsDependencyNode)exOrder[node.Node.Id];
                    if (itemInhash.Level < level)
                    {
                        itemInhash.Level = level;
                    }

                    if (!itemInhash.ParentIds.Contains(parentId))
                    {
                        itemInhash.ParentIds.Add(parentId);
                    }

                }
                else
                {
                    exOrder.Add(sensor.Id, node);
                }

                tree.Add(node);
            }

            return tree;

        }

    }

}
