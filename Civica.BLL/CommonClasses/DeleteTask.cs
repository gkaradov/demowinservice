﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Civica.Infrastructure.BaseClasses;
using Civica.BLL.Models;
using Civica.DBL.DBClasses;
using System.Globalization;
using LumenWorks.Framework.IO.Csv;
using System.IO;
using System.Data;

namespace Civica.BLL.CommonClasses
{

    public class DeleteLine
    {
        public SensorDTO Sensor;
        public DateTimeOffset From;
        public DateTimeOffset To;
        public bool DeleteData;
        public bool DeleteRawData;
    }

    public class DeleteTask
    {



        private static string[] formats = new string[]
        {
            "dd/MM/yyyy hh:mm",
            "dd/MM/yyyy h:mm:ss tt",
            "d/MM/yyyy h:mm:ss tt"
        };

        public ServiceResult<bool> ProcessDeleteFile(FileResult fileRes, MonitoringTaskDTO task)
        {

            List<DeleteLine> filelines = new List<DeleteLine>();
            var res = new ServiceResult<bool>(true);
            String[] sensorline = null;
            int lineCount = 0;
            bool data = false;
            bool rawdata = false;
            DateTime fromtmst;
            DateTime totmst;

            var st = StationDBMethods.GetInactiveStationByStationById(task.MonitoringStationId);
            if (st == null)
            {
                res.DTO = false;
                res.AddError("Process Error", "The station indicated is not valid or active station");
                return res;
            }


            while (fileRes.TextStream.Peek() >= 0)  // loop through the file lines
            {
                var ln = fileRes.TextStream.ReadLine();
                lineCount++;

                if (lineCount == 1)
                {
                    if (!(ln.Substring(0, 1) == "!"))
                    {
                        res.AddServiceLog("Parse Error : ",  "The File has no header line." );
                        res.DTO = false;
                        return res;
                    }
                }

                if (lineCount == 2)
                {
                    if (!(ln.Substring(0, 1) == "!"))
                    {
                        res.AddServiceLog("Parse Error : ", "The File has no column line.");
                        res.DTO = false;
                        return res;
                    }
                }

                if (lineCount > 2)
                {
                    sensorline = ln.Split(',');

                    if (sensorline.Count() < 5)
                    {
                        res.DTO = false;
                        res.AddServiceLog("Warning", "Line # :" + lineCount + " is not valid. It does not have 5 columns ");
                        return res;
                    }

                    // remove any unessesary columns - we only need the first 5
                    if (sensorline.Count() >= 5)
                    {
                        var foos = new List<string>(sensorline);
                        for (var y = 5; y < sensorline.Length; y++)
                        {
                            foos.RemoveAt(5);
                        }
                        sensorline = foos.ToArray();
                    }

                    // check if all 5 columns are empty - if so, skip the line, otherwise, flag it as bad file
                    var isEmptyLine = true;
                    for (var p = 0; p < 5; p++)
                    {
                        if (sensorline[p] != "")
                        {
                            isEmptyLine = false;
                        }
                    }

                    if (isEmptyLine)
                    {
                        continue;
                    }


                    if (sensorline.Count() == 5)
                    {                   
                        int sensorId;
                        if (!int.TryParse(sensorline[0], out sensorId))
                        {
                            res.DTO = false;
                            res.AddError("Parsing Error", "The sensor string is not numeric value at line : " + lineCount);
                            return res;
                        }

                        var sr = SensorDBMethods.GetSensorsBYIds(sensorId);
                        if (sr == null)
                        {
                            res.DTO = false;
                            res.AddError("Process Error", "The sensor indicated is not valid or active sensor at line : " + lineCount);
                            return res;
                        }
                        else
                        {
                            if (sr.Calculated)
                            {
                                res.DTO = false;
                                res.AddError("Process Error", "The sensor indicated is calculated sensor at line : " + lineCount);
                                return res;
                            }
                        }

                        if (sensorline[3] == "1")
                        {
                            data = true;
                        }

                        if (sensorline[4] == "1")
                        {
                            rawdata = true;
                        }

                        if ((data == false) && (rawdata == false))
                        {
                            res.DTO = false;
                            res.AddError("Process Error", "At least one of the delete indicators has to be true for line" + lineCount);
                            return res;
                        }

                        if (!DateTime.TryParse(sensorline[1], out fromtmst))
                        {
                            if (!DateTime.TryParseExact(sensorline[1], formats, CultureInfo.InvariantCulture, DateTimeStyles.None, out fromtmst))
                            {
                                res.ErrorResults.Add("Parsing Error", "The From Time Stamp value  : " + sensorline[1] + " is not a valid dateformst");
                                res.DTO = false;
                                return res;
                            }
                        }
                        var fromts = new DateTimeOffset(fromtmst, new TimeSpan((int)st.TimeZone.Offset, 0, 0));

                        if (!DateTime.TryParse(sensorline[2], out totmst))
                        {
                            if (!DateTime.TryParseExact(sensorline[2], formats, CultureInfo.InvariantCulture, DateTimeStyles.None, out totmst))
                            {
                                res.ErrorResults.Add("Parsing Error", "The To Time Stamp value  : " + sensorline[2] + " is not a valid dateformst");
                                res.DTO = false;
                                return res;
                            }
                        }

                        var tots = new DateTimeOffset(totmst, new TimeSpan((int)st.TimeZone.Offset, 0, 0));

                        if (!(fromtmst < totmst))
                        {
                            res.ErrorResults.Add("Process Error", "The From Time Stamp value  : " + sensorline[1] + " is  greater than the To Time Stamp value :" + sensorline[2]);
                            res.DTO = false;
                            return res;
                        }


                        var csensor = SensorDTO.Create(sr);
                        var line = new DeleteLine();

                        line.Sensor = csensor;
                        line.From = fromts;
                        line.To = tots;
                        line.DeleteData = data;
                        line.DeleteRawData = rawdata;
                        filelines.Add(line);
                    }
                    else
                    {
                        res.DTO = false;
                        res.AddServiceLog("Warning", "Line # :" + lineCount + " is not valid. It does not have 5 columns ");
                        return res;
                    }
                }

            }// end while


            foreach( var fl in filelines)
            {
                var r = ProcessDeleteRequest(task.MonitoringStationId, fl);
                foreach (var er in r.ErrorResults)
                {
                    res.AddServiceLog("Warning at Line : " + lineCount + " : " + er.Key, er.Value);
                }
            }
          

            return res;
        }


        public ServiceResult<bool> ProcessDeleteRequest(int stationId, DeleteLine line)
        {
            var res = new ServiceResult<bool>(true);

            try
            {

                var processData = new DataProcessing();
                var calculatedSensorsMap = new CalculatedSensorMap();
                var updateTimestamps = new List<DateTimeOffset>();
                updateTimestamps = SensorDBMethods.GetSensorTimeRange(line.Sensor.Id, line.From, line.To);

                if (line.DeleteData)
                {
                    var r = SensorDBMethods.DeleteTimeRange(line.Sensor.Id, line.From, line.To);
                    if(!r.DTO)
                    {
                        res.ErrorResults.Add("Database Error", "Could not delete data sensor range for sensor : " + line.Sensor.Id);
                        res.DTO = false;
                    }
                }

                if (line.DeleteRawData)
                {
                    var r = SensorDBMethods.DeleteTimeRange(line.Sensor.Id, line.From, line.To);
                    if (!r.DTO)
                    {
                        res.ErrorResults.Add("Database Error", "Could not delete data raw sensor range for sensor : " + line.Sensor.Id);
                        res.DTO = false;
                    }
                }

                calculatedSensorsMap.Sensors.Add(line.Sensor);
                calculatedSensorsMap.TimeSeries = updateTimestamps;
                var calsResult = processData.BulckUploadCalculatedsensors(calculatedSensorsMap);
                var recalculateEndTimeStamp = SensorDBMethods.RecalculateStationLastTimeStamp(stationId);

                return res;

            }
            catch(Exception ex)
            {
                res.ErrorResults.Add("System Error", ex.Message);
                res.DTO = false;
                return res;
            }
        }
    }

}