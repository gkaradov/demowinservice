﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Civica.BLL.ServiceClasses.Interfaces;
using Civica.Domain.CivicaDomain;
using System.Data.Entity;
using Civica.Infrastructure.BaseClasses;

namespace Civica.BLL.ServiceClasses
{
    public class AlarmDataService
    {
        public datacurrentEntities model;
        public AlarmDataService()
        {
            model = new datacurrentEntities();
        }
        public List<MonitoringStationAlarmCondition> GetAllAlarmConidtions(int alarmId)
        {
           
            var monitoringstationAlarmConditions = model.MonitoringStationAlarmConditions.Where(x => x.MonitoringStationAlarmId == alarmId).Where(x => x.Active == true).ToList();
            return monitoringstationAlarmConditions;
        }
        public List<int> GetAllAlarmSensorDataType(int alarmId)
        {
            var monitoringstationAlarmConditionIds = model.MonitoringStationAlarmConditions.Where(x => x.MonitoringStationAlarmId == alarmId).Where(x => x.Active == true).Select(x => x.Id).ToList();
            var sensorDataTypeIds = model.MonitoringSensorAlarms.Where(x => x.Active == true).Where(x => monitoringstationAlarmConditionIds.Contains(x.MonitoringStationAlarmConditionId)).Select(x => x.MonitoringSensor.SensorDataTypeId).ToList();
            return sensorDataTypeIds;
        }
        public void UpdateAlarmUsePointSensorCalculateLastRun(int alarmId, bool usePointSensor)
        {
            var alarm  = model.MonitoringStationAlarms.Where(x => x.Id == alarmId).FirstOrDefault();
            alarm.UserPointSensorCalculateLastRun = usePointSensor;
            model.SaveChanges();
        }
       
        public List<MonitoringStationAlarm> GetAllAlarms()
        {
           
            var monitoringStationAlarms = model.MonitoringStationAlarms.Where(x => x.Active == true).Where(x => x.TypeId == 1).ToList();
            return monitoringStationAlarms;
        }

        public MonitoringStationAlarmCondition GetMonitoringStationAlarmConditionById(int monitoringstationalarmId)
        {
           
            var monitoringStationAlarm = model.MonitoringStationAlarmConditions.Where(x => x.Id == monitoringstationalarmId).FirstOrDefault();
            return monitoringStationAlarm;
        }

        public MonitoringStationAlarm GetMonitoringStationAlarmById(int alarmId)
        {
            var monitoringStationAlarm = model.MonitoringStationAlarms.Where(x => x.Id == alarmId).FirstOrDefault();
            return monitoringStationAlarm;
        }

        public string GetStationName(int alarmId)
        {
            var stationName = model.MonitoringStationAlarms.Where(x => x.Id == alarmId).FirstOrDefault().MonitoringStation.Name;
            return stationName;
        }

        public string serviceName(int alarmId)
        {
            var serviceName = model.MonitoringStationAlarms.Where(x => x.Id == alarmId).FirstOrDefault().MonitoringStation.MonitoringServiceStations.FirstOrDefault().MonitoringService.Name;
            return serviceName;
        }

        public string stationAlarmSeverity(int alarmId)
        {
            var severity = model.MonitoringStationAlarms.Where(x => x.Id == alarmId).FirstOrDefault().MonitoringStationAlarmSeverity.Type;
            return severity;
        }
        public string getSensorName(int sensorId)
        {
            var sensorName = model.MonitoringSensors.Where(x => x.Id == sensorId).FirstOrDefault().Name;
            return sensorName;

        }
        public List<MonitoringSensor> GetMonitoringConditionMonitoringSensors(int conditionId)
        {
            var MonitoringSensors = model.MonitoringSensorAlarms.Where(x => x.MonitoringStationAlarmConditionId == conditionId).Where(x => x.Active == true).Select(x => x.MonitoringSensor).ToList();
            return MonitoringSensors;
        }
        public List<int> GetMonitoringSensorIdsByAlarmId(int alarmId)
        {
            var monitoringSensorIds = model.MonitoringSensorAlarms.Where(x => x.MonitoringStationAlarmCondition.MonitoringStationAlarmId == alarmId).Where(x => x.Active == true).Select(x => x.MonitoringSensorId).ToList();
            return monitoringSensorIds;
        }

        public void UpdateLastRun(int alarmId, DateTimeOffset? lastRuntime)
        {
            var alarm = model.MonitoringStationAlarms.Where(x => x.Id == alarmId).FirstOrDefault();
            alarm.LastRun = lastRuntime;
            alarm.LastRunCurrentTime = DateTimeOffset.Now;
            model.SaveChanges();
        }

        public void UpdateAlarmSth(bool isTriggered, bool ackStatus, DateTimeOffset? triggeredTime, int alarmId)
        {
            var alarm = model.MonitoringStationAlarms.Where(x => x.Id == alarmId).FirstOrDefault();
            alarm.IsTriggered = isTriggered;
            alarm.AckStatus = ackStatus;
            alarm.TriggeredTime = triggeredTime;
            model.SaveChanges();
        }

        public void UpdateSensorStatus(BaseEnum.AlarmTriggerType trigger, List<int> sensorIds)
        {
            if (sensorIds.Count == 0)
                return;
            int status = 0;
            if (trigger == BaseEnum.AlarmTriggerType.ALARMRES_VIOLATION)
                status = 4;
            else if (trigger == BaseEnum.AlarmTriggerType.ALARMRES_OK)
                status = 7;
            var sensors = model.MonitoringSensors.Where(x => sensorIds.Contains(x.Id));
            foreach (var sensor in sensors)
            {
                sensor.SensorStatusId = status;
            }
            model.SaveChanges();
        }

        public void insertintoLog(int alarmId, string description, string notes)
        {
            MonitoringStationAlarmLog log = new MonitoringStationAlarmLog();
            log.AlarmId = alarmId;
            log.TimeStamp = DateTimeOffset.Now;
            log.description = description;
            log.note = notes;
            log.Active = true;
            model.MonitoringStationAlarmLogs.Add(log);
            model.SaveChanges();

        }


        public void insertInfoIntoAlarmAckinfo(string guid, int alarmId, string emailAddress)
        {
            AlarmAckinfo ackInfo = model.AlarmAckinfoes.Where(x => x.AlarmId == alarmId).Where(x => x.EmailAddress == emailAddress).FirstOrDefault();
            if (ackInfo == null)
            {
                model.AlarmAckinfoes.Add(ackInfo = new AlarmAckinfo());            
                ackInfo.AlarmId = alarmId;
                ackInfo.EmailAddress = emailAddress;
                
            }
            ackInfo.GuidId = guid;
            ackInfo.EmailSendTime = DateTimeOffset.Now;
          var s =  model.SaveChanges();
            model.SaveChanges();

        }
        public void UpdateLastRunCurrentTime(int alarmId)
        {
            var alarm = model.MonitoringStationAlarms.Where(x => x.Id == alarmId).FirstOrDefault();
            alarm.LastRunCurrentTime = DateTimeOffset.Now;
            
            model.SaveChanges();

        }
        public void UpdateNoDataNotificationToTrue(int alarmId)
        {
            var alarm = model.MonitoringStationAlarms.Where(x => x.Id == alarmId).FirstOrDefault();
            alarm.NoDataNotification = true;

            model.SaveChanges();

        }
        public void UpdateNoDataNotificationToFalse(int alarmId)
        {
            var alarm = model.MonitoringStationAlarms.Where(x => x.Id == alarmId).FirstOrDefault();
            alarm.NoDataNotification = false;

            model.SaveChanges();

        }
    }
}
