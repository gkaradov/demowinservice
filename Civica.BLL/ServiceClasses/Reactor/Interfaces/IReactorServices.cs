﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;

namespace Civica.BLL.ServiceClasses.Reactor.Interfaces
{
    public interface IReactorServices
    {
         void StartIPReg(int port, string endpoit, EventLog eLog);

         void StartBEvent(int port, string endpoit, EventLog eLog);
    }

}


