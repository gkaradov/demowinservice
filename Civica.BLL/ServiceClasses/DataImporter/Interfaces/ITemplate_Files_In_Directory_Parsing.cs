﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Civica.Infrastructure.BaseClasses;

namespace Civica.BLL.ServiceClasses.DataImporter.Interfaces
{
    public interface ITemplate_Files_In_Directory_Parsing
    {
        bool StartTemplate(string path, ref DCLogger logger);
    }
}