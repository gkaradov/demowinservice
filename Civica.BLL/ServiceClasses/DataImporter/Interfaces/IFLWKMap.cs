﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NLog;
using Civica.Infrastructure.BaseClasses;

namespace Civica.BLL.ServiceClasses.DataImporter.Interfaces
{
    public interface IFLWKMap
    {
        bool StartFLWKMap(string path, ref DCLogger logger);
    }
}
