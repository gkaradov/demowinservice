﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Civica.Infrastructure.BaseClasses;

namespace Civica.BLL.ServiceClasses.DataImporter.Interfaces
{
    public interface IHOBOUX120
    {
        bool StartHOBOUX120(string path, ref DCLogger logger);
    }
}
