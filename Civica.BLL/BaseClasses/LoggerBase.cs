﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NLog;
using Civica.Infrastructure.BaseClasses;
using Civica.BLL.Models;

namespace Civica.BLL.BaseClasses
{
    public class LoggerBase
    {
        public Logger LogWriter { get; set; }
        public string _filePath { get; set; }

        public List<string> FileList { get; set; }

        public LoggerBase()
        {
            //this.LogWriter = new Logger();

        }


        public DirectoryResult GetFilesInDirectory(string path)
        {
            DirectoryResult res = new DirectoryResult();

            if (Directory.Exists(path))
            {

                // var fileEntries = Directory.GetFiles(path,"*",SearchOption.AllDirectories).OrderBy(d => new FileInfo(d).CreationTime).ToArray(); ;

                List<string> fileEntries = new List<string>();
                var directories = Directory.GetDirectories(path, "*", SearchOption.TopDirectoryOnly);

                if (directories != null)
                {

                    if (directories.Length > 0)
                    {
                        foreach (var orgdir in directories)   /// organization level directory
                        {


                            var orgfiles = Directory.GetFiles(orgdir, "*", SearchOption.TopDirectoryOnly).ToList();

                            if (orgfiles != null)
                            {
                                if (orgfiles.Count() > 0)
                                {
                                    fileEntries.AddRange(orgfiles);

                                    foreach (var of in orgfiles)
                                    {

                                        var file = new DirectoryFiles();
                                        file.FilePath = of;
                                        file.RootDirectory = orgdir;
                                        res.Files.Add(file);

                                    }
                                }
                            }


                            var ldir = Directory.GetDirectories(orgdir, "*", SearchOption.TopDirectoryOnly);


                            if (ldir != null)
                            {

                                foreach (var ld in ldir)   // organizational level subdirectories, if they exists
                                {
                                    if (ld.Contains("ErrorArchive") || ld.Contains("Archive") || ld.Contains("Other"))
                                    {
                                        continue;
                                    }

                                    var dfiles = Directory.GetFiles(ld, "*", SearchOption.AllDirectories).ToList();

                                    if (dfiles != null)
                                    {
                                        if (dfiles.Count() > 0)
                                        {
                                            foreach(var df in dfiles)
                                            {

                                                var file = new DirectoryFiles();
                                                file.FilePath = df;
                                                file.RootDirectory = orgdir;
                                                res.Files.Add(file);

                                            }

                                           // fileEntries.AddRange(dfiles);
                                        }
                                    }
                                }
                            }

                        }

                        if (fileEntries.Count() > 0)
                        {
                            res.Files = res.Files.OrderBy(d => new FileInfo(d.FilePath).CreationTime).ToList();
                            res.FileCount = fileEntries.Count();
                        }
                        else
                        {
                            res.FileCount = 0;
                            res.Messages.Add("No Files found ");
                        }
                    }
                }
                else
                {
                    res.FileCount = 0;
                    res.Messages.Add("No directories found in : " + path);
                }
            }
            else
            {
                res.FileCount = 0;
                res.Messages.Add("Directory Not Found : " + path);
            }

            return res;
        }


        public DirectoryResult GetDetecFilesInDirectory(string path, string uniqueId)
        {
            DirectoryResult res = new DirectoryResult();

            if (Directory.Exists(path))
            {
                var files = Directory.GetFiles(path, uniqueId + "_*.dat", SearchOption.TopDirectoryOnly).ToList();

                if (files != null)
                {
                    if (files.Count() > 0)
                    {
                        res.FileCount = files.Count();

                        foreach (var df in files)
                        {
                            var file = new DirectoryFiles();
                            file.FilePath = df;
                            file.RootDirectory = path;
                            res.Files.Add(file);

                        }

                    }
                }
            }
            else
            {
                res.FileCount = 0;
                res.Messages.Add("Directory Not Found : " + path);
            }

            return res;
        }





        public FileResult GetFile(MonitoringTaskDTO task)
        {
            var res = new FileResult();
            FileStream stream = null;
            res.Exists = File.Exists(task.FilePath);

            if (res.Exists)
            {

                FileInfo f = new FileInfo(task.FilePath);
                try
                {
                    stream = f.Open(FileMode.Open, FileAccess.Read, FileShare.None);

                    res.File = stream;
                }
                catch (IOException ioe)
                {
                    //the file is unavailable because it is:
                    //still being written to
                    //or being processed by another thread
                    //or does not exist (has already been processed)
                    res.File = null;
                    res.Messages.Add("File Error : " + ioe.Message);
                }
                finally
                {
                    if (stream != null)
                        stream.Close();
                }
            }
            else
            {
                res.Messages.Add("File :" + task.FilePath + " does not exists");

            }
            return res;
        }

        public FileResult GetTextFile(MonitoringTaskDTO task)
        {
            var res = new FileResult();
            // FileStream stream = null;
            res.Exists = File.Exists(task.FilePath);

            if (res.Exists)
            {


                try
                {
                    var file = System.IO.File.OpenText(task.FilePath);
                    //  stream = ((FileStream)file.BaseStream); 
                    res.TextStream = file;
                }
                catch (IOException ioe)
                {
                    //the file is unavailable because it is:
                    //still being written to
                    //or being processed by another thread
                    //or does not exist (has already been processed)
                    res.File = null;

                    res.Messages.Add("File Error : " + ioe.Message);
                }
                finally
                {
                    //if (stream != null)
                    //    stream.Close();
                }
            }
            else
            {
                res.Messages.Add("File :" + task.FilePath + " does not exists");

            }
            return res;
        }

    }
}
